package com.hnttechs.www.cityguide;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by dell on 9/28/16.
 */
public class Activity_AboutUs extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("About Us");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");

        TextView txt_about_us = (TextView)findViewById(R.id.txt_about_us);
        txt_about_us.setTypeface(tf_zawgyi);
        txt_about_us.setText("ကြ်န္ေတာ္တို႕ ျပည္လမ္းညႊန္ Mobile Application\n" +
                "မွ လူႀကီးမင္းမ်ားအဆင္ေျပႏိုင္ရန္အတြက္\n" +
                "ျပည္ၿမိဳ႕ရွိ လည္ပတ္ရန္ ေနရာမ်ား၊လုပ္ငန္းရွင္ေပါင္း\n" +
                "၃၀၀၀ေက်ာ္၏ ဆုိင္နာမည္ကုိေဖာ္ျပထားပါသည္း\n" +
                "မိမိအေနျဖင့္  ေ၀ဖန္အၿကံေပးလုိလွ်င္ျဖစ္ေစ၊ဆုိင္လုပ္ငန္းမ်ား\n" +
                "ေၾကာ္ျငာမ်ား ထည့္သြင္းလုိလွ်င္ျဖစ္ေစ\n" +
                "ဖုန္းနံပတ္ - 09788888465\n" +
                "09976883888 သုိ႕ ျဖစ္ေစ\n" +
                "ဘာပဲလုိလုိျပည္လမ္းညႊန္ Facebook Page စာမ်က္ႏွာ သုိ႕ျဖစ္ေစ\n" +
                "ဆက္သြယ္အႀကံျပဳ ေၾကာ္ျငာမ်ား \n ထည့္သြင္းပါသည္။");

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}

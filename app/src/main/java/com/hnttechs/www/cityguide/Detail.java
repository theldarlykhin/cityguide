package com.hnttechs.www.cityguide;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dell on 9/30/16.
 */
public class Detail extends AppCompatActivity {
    private CollapsingToolbarLayout collapsingToolbarLayout;
    static String phone;
    static String phone1;
    static String phone2;
    ImageLoader imageLoader = new ImageLoader(this);
    private SharedPreferences mPreferences;
    static String user_name;
    ProgressDialog dialog;
    static ProgressDialog ringProgressDialog;
    static String serverData;
    ArrayList<Rating> bankArrayList;
    DBHandler handler;
    static ListViewAdapterRating adapter;
    static FullLengthListView lv_rating;
    static String name;
    static String rate_name;
static String category;
    static RelativeLayout layout_rating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        name = intent.getStringExtra("name");
        final String address = intent.getStringExtra("address");
        phone = intent.getStringExtra("phone");
        phone1 = intent.getStringExtra("phone1");
        phone2 = intent.getStringExtra("phone2");
        final String website = intent.getStringExtra("website");
        final String latitude = intent.getStringExtra("latitude");
        final String longitude = intent.getStringExtra("longitude");
        String avatar = intent.getStringExtra("avatar");
        final String avatar1 = intent.getStringExtra("avatar1");
        final String avatar2 = intent.getStringExtra("avatar2");
        final String avatar3 = intent.getStringExtra("avatar3");
        final String avatar4 = intent.getStringExtra("avatar4");
        final String description = intent.getStringExtra("description");
        category= intent.getStringExtra("category");
        mPreferences = getBaseContext().getSharedPreferences("CurrentUser", Context.MODE_PRIVATE);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(name);

        handler = new DBHandler(getBaseContext());
        ImageView main_photo = (ImageView) findViewById(R.id.main_photo);
        ImageView img_detail_photo_1 = (ImageView) findViewById(R.id.img_detail_photo_1);
        ImageView img_detail_photo_2 = (ImageView) findViewById(R.id.img_detail_photo_2);
        ImageView img_detail_photo_3 = (ImageView) findViewById(R.id.img_detail_photo_3);
        ImageView img_detail_photo_4 = (ImageView) findViewById(R.id.img_detail_photo_4);
        ImageView img_home = (ImageView) findViewById(R.id.img_home);
        ImageView img_phone = (ImageView) findViewById(R.id.img_phone);
        ImageView img_phone1 = (ImageView) findViewById(R.id.img_phone2);
        ImageView img_phone2 = (ImageView) findViewById(R.id.img_phone3);
        ImageView img_desc = (ImageView) findViewById(R.id.img_desc);
        ImageView img_map = (ImageView) findViewById(R.id.img_map);
        TextView lbl_address = (TextView) findViewById(R.id.lbl_address);
        final TextView lbl_phone = (TextView) findViewById(R.id.lbl_phone);
        final TextView lbl_phone2 = (TextView) findViewById(R.id.lbl_phone2);
        final TextView lbl_phone3 = (TextView) findViewById(R.id.lbl_phone3);
        TextView lbl_desc = (TextView) findViewById(R.id.lbl_desc);
        TextView lbl_location = (TextView) findViewById(R.id.lbl_location);
        RatingBar rate_me = (RatingBar) findViewById(R.id.rate_me);
        lv_rating = (FullLengthListView) findViewById(R.id.lv_rating);
        layout_rating = (RelativeLayout) findViewById(R.id.layout_rating);

        Typeface font = Typeface.createFromAsset(getBaseContext().getAssets(),
                "fonts/zawgyione.ttf");

        lbl_address.setTypeface(font);
        lbl_phone.setTypeface(font);
        lbl_phone2.setTypeface(font);
        lbl_phone3.setTypeface(font);
        lbl_desc.setTypeface(font);
        lbl_location.setTypeface(font);

        lbl_address.setText(address);
        lbl_phone.setText(phone);
        lbl_phone2.setText(phone1);
        lbl_phone3.setText(phone2);
        lbl_desc.setText(description);
        lbl_location.setText(latitude + "," + longitude);

        Drawable mDrawable_home = getBaseContext().getResources().getDrawable(R.drawable.home);
        mDrawable_home.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#5662ac"), PorterDuff.Mode.MULTIPLY));

        Drawable mDrawable_phone_icon = getBaseContext().getResources().getDrawable(R.drawable.phone_icon);
        mDrawable_phone_icon.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#5662ac"), PorterDuff.Mode.MULTIPLY));

        Drawable mDrawable_web_icon = getBaseContext().getResources().getDrawable(R.drawable.desc);
        mDrawable_web_icon.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#5662ac"), PorterDuff.Mode.MULTIPLY));

        Drawable mDrawable_map_icon = getBaseContext().getResources().getDrawable(R.drawable.map);
        mDrawable_map_icon.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#5662ac"), PorterDuff.Mode.MULTIPLY));


        imageLoader.DisplayImage(avatar, main_photo);
        imageLoader.DisplayImage(avatar1, img_detail_photo_1);
        imageLoader.DisplayImage(avatar2, img_detail_photo_2);
        imageLoader.DisplayImage(avatar3, img_detail_photo_3);
        imageLoader.DisplayImage(avatar4, img_detail_photo_4);

        img_home.setImageDrawable(mDrawable_home);
        img_phone.setImageDrawable(mDrawable_phone_icon);
        img_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + lbl_phone.getText()));
                startActivity(callIntent);
            }
        });
        lbl_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + lbl_phone.getText()));
                startActivity(callIntent);
            }
        });

        img_phone1.setImageDrawable(mDrawable_phone_icon);
        img_phone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + lbl_phone2.getText()));
                startActivity(callIntent);
            }
        });

        lbl_phone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + lbl_phone2.getText()));
                startActivity(callIntent);
            }
        });
        img_phone2.setImageDrawable(mDrawable_phone_icon);
        img_phone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + lbl_phone3.getText()));
                startActivity(callIntent);
            }
        });
        lbl_phone3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + lbl_phone3.getText()));
                startActivity(callIntent);
            }
        });

        img_desc.setImageDrawable(mDrawable_web_icon);


        img_map.setImageDrawable(mDrawable_map_icon);
        img_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude + ""));
                startActivity(intent);
            }
        });

        lbl_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude + ""));
                startActivity(intent);
            }
        });

        img_detail_photo_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog settingsDialog = new Dialog(Detail.this);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog
                        , null));

                ScaleImageView img = (ScaleImageView) settingsDialog.findViewById(R.id.scale_image);
                imageLoader.DisplayImage(avatar1, img);
                settingsDialog.show();
            }
        });

        img_detail_photo_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog settingsDialog = new Dialog(Detail.this);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog
                        , null));

                ScaleImageView img = (ScaleImageView) settingsDialog.findViewById(R.id.scale_image);
                imageLoader.DisplayImage(avatar2, img);
                settingsDialog.show();
            }
        });

        img_detail_photo_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog settingsDialog = new Dialog(Detail.this);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog
                        , null));

                ScaleImageView img = (ScaleImageView) settingsDialog.findViewById(R.id.scale_image);
                imageLoader.DisplayImage(avatar3, img);
                settingsDialog.show();
            }
        });

        img_detail_photo_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog settingsDialog = new Dialog(Detail.this);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog
                        , null));

                ScaleImageView img = (ScaleImageView) settingsDialog.findViewById(R.id.scale_image);
                imageLoader.DisplayImage(avatar4, img);
                settingsDialog.show();
            }
        });

        rate_me.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                if (mPreferences.contains("AuthToken")) {
                    user_name = mPreferences.getString("UserName", "").toString();
                    sendPostRequest(user_name, ratingBar.getRating(), name, category);
                    dialog = ProgressDialog.show(Detail.this, "", "Sending...", true);
                    dialog.show();

                } else {
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(intent);
                    ratingBar.setRating(0.0f);
                }

            }
        });
        if(isInternetOn() == true) {
            new DataFetcherTask().execute();
        } else{
            final ArrayList<Rating> banklist = handler.getAllrating(name, category);
            adapter = new ListViewAdapterRating(getBaseContext(), banklist);
            lv_rating.setAdapter(adapter);
        }

    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getBaseContext().getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }


    private void sendPostRequest(String userName, Float rateNumber, String rateName, String ratecategory) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                String paramUserName = (String) params[0];
                Float paramRateNumber = (Float) params[1];
                String paramRateName = (String) params[2];
                String paramRatecategory = (String) params[3];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://45.120.149.8/to_give_rate");

                BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair("username", paramUserName);
                BasicNameValuePair ratenumberBasicNameValuePAir = new BasicNameValuePair("rate_number", paramRateNumber.toString());
                BasicNameValuePair ratenameBasicNameValuePAir = new BasicNameValuePair("rate_name", paramRateName);
                BasicNameValuePair ratecategoryBasicNameValuePAir = new BasicNameValuePair("rate_category" , paramRatecategory);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(usernameBasicNameValuePair);
                nameValuePairList.add(ratenumberBasicNameValuePAir);
                nameValuePairList.add(ratenameBasicNameValuePAir);
                nameValuePairList.add(ratecategoryBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                dialog.dismiss();
                new DataFetcherTask().execute();

            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(userName, rateNumber, rateName, ratecategory);
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ringProgressDialog = ProgressDialog.show(Detail.this, "Please wait ...", "Updating ...", true);
            ringProgressDialog.setCancelable(true);

        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;
            String newsUrl;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://45.120.149.8/my_bank_rating.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity, HTTP.UTF_8);
                Log.d("response", serverData);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                bankArrayList = new ArrayList<Rating>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Rating");

                handler.deleteData("rating");


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectNews = jsonArray.getJSONObject(i);
                    String  username= jsonObjectNews.getString("username");
                    String rate_number = jsonObjectNews.getString("rate_number");
                    String rate_name = jsonObjectNews.getString("rate_name");
                    String rate_category = jsonObjectNews.getString("rate_category");

                    Rating bank = new Rating();
                    bank.setUser_name(username);
                    bank.setRate_name(rate_name);
                    bank.setRate_number(rate_number);
                    bank.setRate_category(rate_category);

                    handler.addrate(bank);// Inserting into DB

                }
                rate_name = jsonArray.getJSONObject(0).getString("rate_name");

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end
            //Json Parsing code end
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ringProgressDialog.dismiss();
            final ArrayList<Rating> banklist = handler.getAllrating(name, category);
            adapter = new ListViewAdapterRating(getBaseContext(), banklist);
            lv_rating.setAdapter(adapter);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}

package com.hnttechs.www.cityguide;

/**
 * Created by dell on 8/20/16.
 */
public class Farmer_shop {
    private int id;
    private String name;
    private String address;
    private String phone;
    private String phone1;
    private String phone2;
    private String website;
    private String latitude;
    private String longitude;
    private String avatar;
    private String avatar1;
    private String avatar2;
    private String avatar3;
    private String avatar4;
    private String desc;

    public Farmer_shop() {
    }

    public Farmer_shop(String name, String address, String phone, String phone1,String phone2, String website, String latitude,
                       String longitude,String avatar,String avatar1,String avatar2,String avatar3,String avatar4, String desc) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.website = website;
        this.latitude = latitude;
        this.longitude = longitude;
        this.avatar = avatar;
        this.avatar1 =avatar1;
        this.avatar2 = avatar2;
        this.avatar3=avatar3;
        this.avatar4=avatar4;
        this.desc = desc;
    }

    public Farmer_shop(int id, String name, String address, String phone,String phone1,String phone2,  String website, String latitude,
                       String longitude,String avatar,String avatar1,String avatar2,String avatar3,String avatar4, String desc) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.website = website;
        this.latitude = latitude;
        this.longitude = longitude;
        this.avatar = avatar;
        this.avatar1 =avatar1;
        this.avatar2 = avatar2;
        this.avatar3=avatar3;
        this.avatar4=avatar4;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address= address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone= phone;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website= website;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar1() {
        return avatar1;
    }

    public void setAvatar1(String avatar1) {
        this.avatar1 = avatar1;
    }

    public String getAvatar2() {
        return avatar2;
    }

    public void setAvatar2(String avatar2) {
        this.avatar2 = avatar2;
    }

    public String getAvatar3() {
        return avatar3;
    }

    public void setAvatar3(String avatar3) {
        this.avatar3 = avatar3;
    }

    public String getAvatar4() {
        return avatar4;
    }

    public void setAvatar4(String avatar4) {
        this.avatar4 = avatar4;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc= desc;
    }

}

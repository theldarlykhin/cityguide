package com.hnttechs.www.cityguide;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dell on 9/29/16.
 */
public class ActivitySearchByCategoryList extends ActionBarActivity {

    private Toolbar toolbar;
    Handler updateBarHandler;
    static List<String> SPINNER_DATA = new ArrayList<String>();
    public int currentimageindex = 0;
    ImageView slidingimage;
    static String shop_name;
    DBHandler handler;
    static String serverData;
    ArrayList<Hotel> hotelArrayList;
    static hotelListViewAdapter hotel_adapter;
    static FullLengthListView listview;
    static ProgressDialog ringProgressDialog;
    static String category_name;
    static ArrayList<Ads> ads_list;
    ImageLoader imageLoader = new ImageLoader(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchby_category_list);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.actionbar_hotel);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        final Spinner spn_hotel_category = (Spinner) findViewById(R.id.spn_hotel_category);
        slidingimage = (ImageView) findViewById(R.id.img_advertisement);
        listview = (FullLengthListView) findViewById(R.id.listview);
        listview.setFocusable(false);

        category_name = "အမ်ိဳးအစားေရြးရန္";
        SPINNER_DATA.clear();
        SPINNER_DATA.add("အမ်ိဳးအစားေရြးရန္");
        SPINNER_DATA.add("ဟိုတယ္");
        SPINNER_DATA.add("တည္းခိုခန္း");
        SPINNER_DATA.add("မိုတယ္");

        handler = new DBHandler(getBaseContext());


        if (isInternetOn() == true) {
            new DataFetcherTask().execute();
        } else {
            Toast.makeText(getBaseContext(), "No Internet Access", Toast.LENGTH_SHORT).show();

            ringProgressDialog = ProgressDialog.show(ActivitySearchByCategoryList.this, "Please wait ...", "Updating ...", true);
            ringProgressDialog.setCancelable(false);

            final ArrayList<Hotel> hotellist = handler.getAllhotel();
            hotel_adapter = new hotelListViewAdapter(getBaseContext(), hotellist);
            listview.setAdapter(hotel_adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                    detail_intent.putExtra("name", hotellist.get(position).getName().toString());
                    detail_intent.putExtra("address", hotellist.get(position).getAddress().toString());
                    detail_intent.putExtra("phone", hotellist.get(position).getPhone().toString());
                    detail_intent.putExtra("phone1", hotellist.get(position).getPhone1().toString());
                    detail_intent.putExtra("phone2", hotellist.get(position).getPhone2().toString());
                    detail_intent.putExtra("website", hotellist.get(position).getWebsite().toString());
                    detail_intent.putExtra("latitude", hotellist.get(position).getLatitude().toString());
                    detail_intent.putExtra("longitude", hotellist.get(position).getLongitude().toString());
                    detail_intent.putExtra("avatar", hotellist.get(position).getAvatar().toString());
                    detail_intent.putExtra("avatar1", hotellist.get(position).getAvatar1().toString());
                    detail_intent.putExtra("avatar2", hotellist.get(position).getAvatar2().toString());
                    detail_intent.putExtra("avatar3", hotellist.get(position).getAvatar3().toString());
                    detail_intent.putExtra("avatar4", hotellist.get(position).getAvatar4().toString());
                    detail_intent.putExtra("description", hotellist.get(position).getDesc().toString());
                    detail_intent.putExtra("category", "hotel");
                    startActivity(detail_intent);
                }
            });
            ringProgressDialog.dismiss();


            ads_list = handler.getAlladsbyCategory("hotel");

            final android.os.Handler mHandler = new android.os.Handler();
            final Runnable mUpdateResults = new Runnable() {
                public void run() {
                    if (ads_list.size() != 0) {
                        AnimateandSlideShow();
                    }
                }
            };

            int delay = 500; // delay for 1 sec.
            int period = 1500; // repeat every 4 sec.
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    mHandler.post(mUpdateResults);
                }
            }, delay, period);

            slidingimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ads_list.size() > 0) {
                        final ArrayList<Hotel> clickedbanklist = handler.getHotelDetailbyshop_id(shop_name);
                        if (clickedbanklist.size() > 0) {
                            Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                            detail_intent.putExtra("name", clickedbanklist.get(0).getName().toString());
                            detail_intent.putExtra("address", clickedbanklist.get(0).getAddress().toString());
                            detail_intent.putExtra("phone", clickedbanklist.get(0).getPhone().toString());
                            detail_intent.putExtra("phone1", clickedbanklist.get(0).getPhone1().toString());
                            detail_intent.putExtra("phone2", clickedbanklist.get(0).getPhone2().toString());
                            detail_intent.putExtra("website", clickedbanklist.get(0).getWebsite().toString());
                            detail_intent.putExtra("latitude", clickedbanklist.get(0).getLatitude().toString());
                            detail_intent.putExtra("longitude", clickedbanklist.get(0).getLongitude().toString());
                            detail_intent.putExtra("avatar", clickedbanklist.get(0).getAvatar().toString());
                            detail_intent.putExtra("avatar1", clickedbanklist.get(0).getAvatar1().toString());
                            detail_intent.putExtra("avatar2", clickedbanklist.get(0).getAvatar2().toString());
                            detail_intent.putExtra("avatar3", clickedbanklist.get(0).getAvatar3().toString());
                            detail_intent.putExtra("avatar4", clickedbanklist.get(0).getAvatar4().toString());
                            detail_intent.putExtra("description", clickedbanklist.get(0).getDesc().toString());
                            detail_intent.putExtra("category", "hotel");
                            startActivity(detail_intent);
                        } else {
                            Toast.makeText(getBaseContext(), "There is no data matching your ads name.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }


        fill_data_to_spinner_with_arraylist(SPINNER_DATA, spn_hotel_category);

        spn_hotel_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                ringProgressDialog = ProgressDialog.show(ActivitySearchByCategoryList.this, "Please wait ...", "Updating ...", true);
//                ringProgressDialog.setCancelable(false);

                if (spn_hotel_category.getSelectedItem().toString().equals("ဟိုတယ္")) {
                    category_name = "ဟိုတယ္";

                    final ArrayList<Hotel> hotellist = handler.gethotelbycategory(category_name);
                    hotel_adapter = new hotelListViewAdapter(getBaseContext(), hotellist);
                    listview.setAdapter(hotel_adapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                            detail_intent.putExtra("name", hotellist.get(position).getName().toString());
                            detail_intent.putExtra("address", hotellist.get(position).getAddress().toString());
                            detail_intent.putExtra("phone", hotellist.get(position).getPhone().toString());
                            detail_intent.putExtra("phone1", hotellist.get(position).getPhone1().toString());
                            detail_intent.putExtra("phone2", hotellist.get(position).getPhone2().toString());
                            detail_intent.putExtra("website", hotellist.get(position).getWebsite().toString());
                            detail_intent.putExtra("latitude", hotellist.get(position).getLatitude().toString());
                            detail_intent.putExtra("longitude", hotellist.get(position).getLongitude().toString());
                            detail_intent.putExtra("avatar", hotellist.get(position).getAvatar().toString());
                            detail_intent.putExtra("avatar1", hotellist.get(position).getAvatar1().toString());
                            detail_intent.putExtra("avatar2", hotellist.get(position).getAvatar2().toString());
                            detail_intent.putExtra("avatar3", hotellist.get(position).getAvatar3().toString());
                            detail_intent.putExtra("avatar4", hotellist.get(position).getAvatar4().toString());
                            detail_intent.putExtra("description", hotellist.get(position).getDesc().toString());
                            detail_intent.putExtra("category", "hotel");
                            startActivity(detail_intent);
                        }
                    });
                } else if (spn_hotel_category.getSelectedItem().toString().equals("တည္းခိုခန္း")) {

                    category_name = "တည္းခိုခန္း";

                    final ArrayList<Hotel> hotellist = handler.gethotelbycategory(category_name);
                    hotel_adapter = new hotelListViewAdapter(getBaseContext(), hotellist);
                    listview.setAdapter(hotel_adapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                            detail_intent.putExtra("name", hotellist.get(position).getName().toString());
                            detail_intent.putExtra("address", hotellist.get(position).getAddress().toString());
                            detail_intent.putExtra("phone", hotellist.get(position).getPhone().toString());
                            detail_intent.putExtra("phone1", hotellist.get(position).getPhone1().toString());
                            detail_intent.putExtra("phone2", hotellist.get(position).getPhone2().toString());
                            detail_intent.putExtra("website", hotellist.get(position).getWebsite().toString());
                            detail_intent.putExtra("latitude", hotellist.get(position).getLatitude().toString());
                            detail_intent.putExtra("longitude", hotellist.get(position).getLongitude().toString());
                            detail_intent.putExtra("avatar", hotellist.get(position).getAvatar().toString());
                            detail_intent.putExtra("avatar1", hotellist.get(position).getAvatar1().toString());
                            detail_intent.putExtra("avatar2", hotellist.get(position).getAvatar2().toString());
                            detail_intent.putExtra("avatar3", hotellist.get(position).getAvatar3().toString());
                            detail_intent.putExtra("avatar4", hotellist.get(position).getAvatar4().toString());
                            detail_intent.putExtra("description", hotellist.get(position).getDesc().toString());
                            detail_intent.putExtra("category", "hotel");
                            startActivity(detail_intent);
                        }
                    });
                } else if (spn_hotel_category.getSelectedItem().toString().equals("မိုတယ္")) {

                    category_name = "မိုတယ္";

                    final ArrayList<Hotel> hotellist = handler.gethotelbycategory(category_name);
                    hotel_adapter = new hotelListViewAdapter(getBaseContext(), hotellist);
                    listview.setAdapter(hotel_adapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                            detail_intent.putExtra("name", hotellist.get(position).getName().toString());
                            detail_intent.putExtra("address", hotellist.get(position).getAddress().toString());
                            detail_intent.putExtra("phone", hotellist.get(position).getPhone().toString());
                            detail_intent.putExtra("phone1", hotellist.get(position).getPhone1().toString());
                            detail_intent.putExtra("phone2", hotellist.get(position).getPhone2().toString());
                            detail_intent.putExtra("website", hotellist.get(position).getWebsite().toString());
                            detail_intent.putExtra("latitude", hotellist.get(position).getLatitude().toString());
                            detail_intent.putExtra("longitude", hotellist.get(position).getLongitude().toString());
                            detail_intent.putExtra("avatar", hotellist.get(position).getAvatar().toString());
                            detail_intent.putExtra("avatar1", hotellist.get(position).getAvatar1().toString());
                            detail_intent.putExtra("avatar2", hotellist.get(position).getAvatar2().toString());
                            detail_intent.putExtra("avatar3", hotellist.get(position).getAvatar3().toString());
                            detail_intent.putExtra("avatar4", hotellist.get(position).getAvatar4().toString());
                            detail_intent.putExtra("description", hotellist.get(position).getDesc().toString());
                            detail_intent.putExtra("category", "hotel");
                            startActivity(detail_intent);
                        }
                    });
                } else if (spn_hotel_category.getSelectedItem().toString().equals("အမ်ိဳးအစားေရြးရန္")) {

                    final ArrayList<Hotel> hotellist = handler.getAllhotel();
                    hotel_adapter = new hotelListViewAdapter(getBaseContext(), hotellist);
                    listview.setAdapter(hotel_adapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                            detail_intent.putExtra("name", hotellist.get(position).getName().toString());
                            detail_intent.putExtra("address", hotellist.get(position).getAddress().toString());
                            detail_intent.putExtra("phone", hotellist.get(position).getPhone().toString());
                            detail_intent.putExtra("phone1", hotellist.get(position).getPhone1().toString());
                            detail_intent.putExtra("phone2", hotellist.get(position).getPhone2().toString());
                            detail_intent.putExtra("website", hotellist.get(position).getWebsite().toString());
                            detail_intent.putExtra("latitude", hotellist.get(position).getLatitude().toString());
                            detail_intent.putExtra("longitude", hotellist.get(position).getLongitude().toString());
                            detail_intent.putExtra("avatar", hotellist.get(position).getAvatar().toString());
                            detail_intent.putExtra("avatar1", hotellist.get(position).getAvatar1().toString());
                            detail_intent.putExtra("avatar2", hotellist.get(position).getAvatar2().toString());
                            detail_intent.putExtra("avatar3", hotellist.get(position).getAvatar3().toString());
                            detail_intent.putExtra("avatar4", hotellist.get(position).getAvatar4().toString());
                            detail_intent.putExtra("description", hotellist.get(position).getDesc().toString());
                            detail_intent.putExtra("category", "hotel");
                            startActivity(detail_intent);
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ringProgressDialog = ProgressDialog.show(ActivitySearchByCategoryList.this, "Please wait ...", "Updating ...", true);
            ringProgressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;
            String newsUrl;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://45.120.149.8/my_hotel.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                hotelArrayList = new ArrayList<Hotel>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("Hotel");
                JSONArray jsonAdsArray = jsonObject.getJSONArray("Advertisement");


                handler.deleteData("hotel");
                handler.deleteData("ads_hotel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectNews = jsonArray.getJSONObject(i);
                    String name = jsonObjectNews.getString("name");
                    String address = jsonObjectNews.getString("address");
                    String phone = jsonObjectNews.getString("phone");
                    String phone1 = jsonObjectNews.getString("option_phone1");
                    String phone2 = jsonObjectNews.getString("option_phone2");
                    String website = jsonObjectNews.getString("website");
                    String latitude = jsonObjectNews.getString("latitude");
                    String longitude = jsonObjectNews.getString("longitude");
                    String hoteltype = jsonObjectNews.getString("hoteltype");
                    String avatar = "http://45.120.149.8/" + jsonObjectNews.getString("avatar_url5");
                    String avatar1 = "http://45.120.149.8/" + jsonObjectNews.getString("avatar_url1");
                    String avatar2 = "http://45.120.149.8/" + jsonObjectNews.getString("avatar_url2");
                    String avatar3 = "http://45.120.149.8/" + jsonObjectNews.getString("avatar_url3");
                    String avatar4 = "http://45.120.149.8/" + jsonObjectNews.getString("avatar_url4");

                    String description = jsonObjectNews.getString("description");
                    Hotel hotel = new Hotel();
                    hotel.setName(name);
                    hotel.setAddress(address);
                    hotel.setPhone(phone);
                    hotel.setPhone1(phone1);
                    hotel.setPhone2(phone2);
                    hotel.setWebsite(website);
                    hotel.setLatitude(latitude);
                    hotel.setLongitude(longitude);
                    hotel.setHoteltype(hoteltype);
                    hotel.setAvatar(avatar);
                    hotel.setAvatar1(avatar1);
                    hotel.setAvatar2(avatar2);
                    hotel.setAvatar3(avatar3);
                    hotel.setAvatar4(avatar4);
                    hotel.setDesc(description);

                    handler.addhotel(hotel);// Inserting into DB
                }
                for (int i = 0; i < jsonAdsArray.length(); i++) {
                    JSONObject jsonObjectNews = jsonAdsArray.getJSONObject(i);
                    String shop_id = jsonObjectNews.getString("shop_id");
                    String category = jsonObjectNews.getString("category");
                    String avatar = "http://45.120.149.8" + jsonObjectNews.getString("avatar_url");

                    Ads bank = new Ads();
                    bank.setShop_id(shop_id);
                    bank.setCategory_id(category);
                    bank.setAvatar(avatar);

                    handler.addads(bank);// Inserting into DB
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            final ArrayList<Hotel> hotellist = handler.getAllhotel();
            hotel_adapter = new hotelListViewAdapter(getBaseContext(), hotellist);
            listview.setAdapter(hotel_adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                    detail_intent.putExtra("name", hotellist.get(position).getName().toString());
                    detail_intent.putExtra("address", hotellist.get(position).getAddress().toString());
                    detail_intent.putExtra("phone", hotellist.get(position).getPhone().toString());
                    detail_intent.putExtra("phone1", hotellist.get(position).getPhone1().toString());
                    detail_intent.putExtra("phone2", hotellist.get(position).getPhone2().toString());
                    detail_intent.putExtra("website", hotellist.get(position).getWebsite().toString());
                    detail_intent.putExtra("latitude", hotellist.get(position).getLatitude().toString());
                    detail_intent.putExtra("longitude", hotellist.get(position).getLongitude().toString());
                    detail_intent.putExtra("avatar", hotellist.get(position).getAvatar().toString());
                    detail_intent.putExtra("avatar1", hotellist.get(position).getAvatar1().toString());
                    detail_intent.putExtra("avatar2", hotellist.get(position).getAvatar2().toString());
                    detail_intent.putExtra("avatar3", hotellist.get(position).getAvatar3().toString());
                    detail_intent.putExtra("avatar4", hotellist.get(position).getAvatar4().toString());
                    detail_intent.putExtra("description", hotellist.get(position).getDesc().toString());
                    detail_intent.putExtra("category", "hotel");
                    startActivity(detail_intent);
                }
            });

            ads_list = handler.getAlladsbyCategory("hotel");

            final android.os.Handler mHandler = new android.os.Handler();
            final Runnable mUpdateResults = new Runnable() {
                public void run() {
                    if (ads_list.size() != 0) {
                        AnimateandSlideShow();
                    }
                }
            };

            int delay = 500; // delay for 1 sec.
            int period = 1500; // repeat every 4 sec.
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    mHandler.post(mUpdateResults);
                }
            }, delay, period);
            slidingimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ads_list.size() > 0) {
                        final ArrayList<Hotel> clickedbanklist = handler.getHotelDetailbyshop_id(shop_name);
                        if (clickedbanklist.size() > 0) {
                            Intent detail_intent = new Intent(getBaseContext(), Detail.class);
                            detail_intent.putExtra("name", clickedbanklist.get(0).getName().toString());
                            detail_intent.putExtra("address", clickedbanklist.get(0).getAddress().toString());
                            detail_intent.putExtra("phone", clickedbanklist.get(0).getPhone().toString());
                            detail_intent.putExtra("phone1", clickedbanklist.get(0).getPhone1().toString());
                            detail_intent.putExtra("phone2", clickedbanklist.get(0).getPhone2().toString());
                            detail_intent.putExtra("website", clickedbanklist.get(0).getWebsite().toString());
                            detail_intent.putExtra("latitude", clickedbanklist.get(0).getLatitude().toString());
                            detail_intent.putExtra("longitude", clickedbanklist.get(0).getLongitude().toString());
                            detail_intent.putExtra("avatar", clickedbanklist.get(0).getAvatar().toString());
                            detail_intent.putExtra("avatar1", clickedbanklist.get(0).getAvatar1().toString());
                            detail_intent.putExtra("avatar2", clickedbanklist.get(0).getAvatar2().toString());
                            detail_intent.putExtra("avatar3", clickedbanklist.get(0).getAvatar3().toString());
                            detail_intent.putExtra("avatar4", clickedbanklist.get(0).getAvatar4().toString());
                            detail_intent.putExtra("description", clickedbanklist.get(0).getDesc().toString());
                            detail_intent.putExtra("category", "hotel");
                            startActivity(detail_intent);
                        } else {
                            Toast.makeText(getBaseContext(), "There is no data matching your ads name.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            ringProgressDialog.dismiss();
        }
    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getBaseContext().getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }

    private void AnimateandSlideShow() {
        imageLoader.DisplayImage(ads_list.get(currentimageindex % ads_list.size()).getAvatar(), slidingimage);
        shop_name = ads_list.get(currentimageindex % ads_list.size()).getShop_id();
        currentimageindex++;
    }

    public void fill_data_to_spinner_with_arraylist(List<String> spinnerdata, Spinner spinnerName) {
        Zawgyi_SpinnerAdapter adapter = new Zawgyi_SpinnerAdapter(getBaseContext(), android.R.layout.simple_spinner_item, spinnerdata);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerName.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}



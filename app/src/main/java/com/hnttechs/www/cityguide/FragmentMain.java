package com.hnttechs.www.cityguide;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class FragmentMain extends Fragment {

    HeaderGridView gridView;
    static final String[] GRID_DATA = new String[]{
            "ဘဏ္ႏွင့္ ATM",
            "ကြန္ပ်ဴတာ၊ မိုဘိုုင္းဖုန္း၊ ဓါတ္ပံုုဆိုုင္၊ မိတၱဴဆိုုင္မ်ား",
            "ဟိုတယ္၊ တည္းခိုခန္း၊ မိုတယ္",
            "ပညာေရးႏွင့္ဆိုုင္ေသာ သင္တန္းမ်ား",
            " ေဆးရံုုႏွင့္ ေဆးခန္းမ်ား၊ ေဆးဆိုုင္မ်ား",
            "အေထြေထြေရာင္းဝယ္ေရး ႏွင့္ မုုန္႕မ်ိဳးစံုုဆိုုင္မ်ား",
            " ျပည္ၿမိဳ႕ရွိ အထင္ကရေနရာမ်ား",
            "စက္သံုုးဆီအေရာင္း ဆိုုင္မ်ား",
            "စိန္၊ ေရႊ၊ ေငြ အေရာင္းဆိုင္မ်ား",
            "Shopping Center ႏွင့္ အထည္ဆိုင္မ်ား ",
            "အေရးေပၚဖုုန္းနံပါတ္မ်ား",
            "လယ္ယာစိုက္ပ်ိဳးေရးပစၥည္းေရာင္း/ငွါးႏွင့္ အိမ္ေဆာက္ပစၥည္းဆိုင္မ်ား",
            "Beauty Saloon မ်ား",
            "အေဝးေျပးကားလိုုင္းမ်ား",
            "Electronic ဆိုုင္မ်ားႏွင့္ စတိုုးဆိုုင္မ်ား",
            "လက္ဖက္ရည္ႏွင့္ စားဖြယ္စံုု",
            "ဆိုင္ကယ္/ကားအေရာင္းဆိုင္ႏွင့္ ကားပစၥည္းဆိုင္မ်ား ",
            "Restaurants & Beer Bar	"

    };

    static final int[] GRID_IMAGE = new int[]{
            R.drawable.bank_icon,
            R.drawable.computer_shop_icon,
            R.drawable.hotel_icon,
            R.drawable.school_icon,
            R.drawable.hospital_icon,
            R.drawable.store_icon,
            R.drawable.famous_place_icon,
            R.drawable.gas_station_icon,
            R.drawable.jewellery_store_icon,
            R.drawable.shoppingmall_icon,
            R.drawable.important_phone_icon,
            R.drawable.farmer_icon,
            R.drawable.beauty_saloon_icon,
            R.drawable.bus_icon,
            R.drawable.electronic_store_icon,
            R.drawable.teashop_icon,
            R.drawable.cycle_icon,
            R.drawable.restaurant_icon
    };

    static final int[] ADS_IMAGE = new int[]{
            R.drawable.place_holder,
            R.drawable.place_holder2,
            R.drawable.place_holder3
    };
    static int currentimageindex = 0;
    static int currentimageindex_stickey = 0;
    ImageView slidingimage;
    static ArrayList<Ads> ads_list;
    static ArrayList<StickeyAds> stickey_ads_list;
    ImageLoader imageLoader = new ImageLoader(getActivity());
    static String serverData;
    static String serverData_stickey;
    ArrayList<Ads> bankArrayList;
    ArrayList<StickeyAds> stickeybankArrayList;
    DBHandler handler;
    static ImageView stickey_ads;
    static String shop_name;
    static String table_name;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_main, container, false);

        gridView = (HeaderGridView) rootview.findViewById(R.id.gridView1);

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View headerView = layoutInflater.inflate(R.layout.homescreen_gridview_header, null);
        slidingimage = (ImageView) headerView.findViewById(R.id.img_advertisement);
        stickey_ads = (ImageView) rootview.findViewById(R.id.stickey_ads);
        gridView.addHeaderView(headerView);
        gridView.setAdapter(new GridViewAdapter(getActivity(), GRID_DATA, GRID_IMAGE));
        gridView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id) {
                        if (position - 2 == 0) {
                            Intent intent = new Intent(getActivity(), ActivityBank.class);
                            startActivity(intent);
                        } else if (position - 2 == 1) {
                            Intent intent = new Intent(getActivity(), ActivityComputer.class);
                            startActivity(intent);
                        } else if (position - 2 == 2) {
                            Intent intent1 = new Intent(getActivity(), ActivitySearchByCategoryList.class);
                            startActivity(intent1);
                        } else if (position - 2 == 3) {
                            Intent intent = new Intent(getActivity(), ActivityEducation.class);
                            startActivity(intent);
                        } else if (position - 2 == 4) {
                            Intent intent = new Intent(getActivity(), ActivityHospital.class);
                            startActivity(intent);
                        } else if (position - 2 == 5) {
                            Intent intent = new Intent(getActivity(), ActivityStore.class);
                            startActivity(intent);
                        } else if (position - 2 == 6) {
                            Intent intent = new Intent(getActivity(), ActivityFamousPlace.class);
                            startActivity(intent);
                        } else if (position - 2 == 7) {
                            Intent intent = new Intent(getActivity(), ActivityGasStation.class);
                            startActivity(intent);
                        } else if (position - 2 == 8) {
                            Intent intent1 = new Intent(getActivity(), ActivityJewellery.class);
                            startActivity(intent1);
                        } else if (position - 2 == 9) {
                            Intent intent = new Intent(getActivity(), ActivityShoppingCenter.class);
                            startActivity(intent);
                        } else if (position - 2 == 10) {
                            Intent intent = new Intent(getActivity(), ActivityEmergencyContact.class);
                            startActivity(intent);
                        } else if (position - 2 == 11) {
                            Intent intent = new Intent(getActivity(), ActivityFarmerShop.class);
                            startActivity(intent);
                        } else if (position - 2 == 12) {
                            Intent intent = new Intent(getActivity(), ActivityBeautySaloon.class);
                            startActivity(intent);
                        } else if (position - 2 == 13) {
                            Intent intent1 = new Intent(getActivity(), ActivityBusGate.class);
                            startActivity(intent1);
                        } else if (position - 2 == 14) {
                            Intent intent = new Intent(getActivity(), ActivityElectronicShop.class);
                            startActivity(intent);
                        } else if (position - 2 == 15) {
                            Intent intent = new Intent(getActivity(), ActivityTeaShop.class);
                            startActivity(intent);
                        } else if (position - 2 == 16) {
                            Intent intent = new Intent(getActivity(), ActivityCycleShop.class);
                            startActivity(intent);
                        } else if (position - 2 == 17) {
                            Intent intent = new Intent(getActivity(), ActivityRestaurant.class);
                            startActivity(intent);
                        }
                    }
                });

        handler = new DBHandler(getActivity());

        if (isInternetOn() == true) {
            new DataFetcherTask().execute();

        } else {

            ads_list = handler.getMainadsbyCategory();
            stickey_ads_list = handler.getAllstickeyads();

            final android.os.Handler mHandler = new android.os.Handler();
            final Runnable mUpdateResults = new Runnable() {
                public void run() {
                    if (ads_list.size() > 0) {
                        AnimateandSlideShow();
                    }
                    if (stickey_ads_list.size() > 0) {
                        AnimateandSlideShow_stickey();
                    }
                }
            };

            int delay = 500; // delay for 1 sec.
            int period = 1500; // repeat every 4 sec.
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    mHandler.post(mUpdateResults);
                }
            }, delay, period);


            slidingimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ads_list.size() > 0) {

                        if (table_name.equals("bank")) {
                            final ArrayList<Bank> clickedbanklist = handler.getBankDetailbyshop_id(shop_name);

                            if (clickedbanklist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedbanklist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedbanklist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedbanklist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedbanklist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedbanklist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedbanklist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedbanklist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedbanklist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedbanklist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedbanklist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedbanklist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedbanklist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedbanklist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedbanklist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "bank");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("beauty_saloon")) {

                            final ArrayList<Beauty_saloon> clickedbeautylist = handler.getBeautySaloonDetailbyshop_id(shop_name);

                            if (clickedbeautylist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedbeautylist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedbeautylist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedbeautylist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedbeautylist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedbeautylist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedbeautylist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedbeautylist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedbeautylist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedbeautylist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedbeautylist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedbeautylist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedbeautylist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedbeautylist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedbeautylist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "beauty_saloon");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }

                        } else if (table_name.equals("bus")) {

                            final ArrayList<Bus_gate> clickedbuslist = handler.getBusGateDetailbyshop_id(shop_name);
                            if (clickedbuslist.size() > 0) {

                                Intent detail_intent = new Intent(getActivity(), Bus_Gate_Detail.class);
                                detail_intent.putExtra("name", clickedbuslist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedbuslist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedbuslist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedbuslist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedbuslist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedbuslist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedbuslist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedbuslist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedbuslist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedbuslist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedbuslist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedbuslist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedbuslist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedbuslist.get(0).getDesc().toString());
                                detail_intent.putExtra("from_township", clickedbuslist.get(0).getFrom_township().toString());
                                detail_intent.putExtra("to_township", clickedbuslist.get(0).getTo_township().toString());
                                detail_intent.putExtra("departure_time", clickedbuslist.get(0).getDeparture_time().toString());
                                detail_intent.putExtra("category", "bus");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("computer")) {
                            final ArrayList<Computer> clickedcomputerlist = handler.getComputerDetailbyshop_id(shop_name);

                            if (clickedcomputerlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedcomputerlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedcomputerlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedcomputerlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedcomputerlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedcomputerlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedcomputerlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedcomputerlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedcomputerlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedcomputerlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedcomputerlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedcomputerlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedcomputerlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedcomputerlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedcomputerlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "computer");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("cycle")) {
                            final ArrayList<Cycle_shop> clickedcyclelist = handler.getCycleShopDetailbyshop_id(shop_name);

                            if (clickedcyclelist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedcyclelist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedcyclelist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedcyclelist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedcyclelist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedcyclelist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedcyclelist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedcyclelist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedcyclelist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedcyclelist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedcyclelist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedcyclelist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedcyclelist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedcyclelist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedcyclelist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "cycle");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("education")) {

                            final ArrayList<Education> clickededucationlist = handler.getEducationDetailbyshop_id(shop_name);

                            if (clickededucationlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickededucationlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickededucationlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickededucationlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickededucationlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickededucationlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickededucationlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickededucationlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickededucationlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickededucationlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickededucationlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickededucationlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickededucationlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickededucationlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickededucationlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "education");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("electronic")) {
                            final ArrayList<Electronic_store> clickedelectroniclist = handler.getElectronicShopDetailbyshop_id(shop_name);

                            if (clickedelectroniclist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedelectroniclist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedelectroniclist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedelectroniclist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedelectroniclist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedelectroniclist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedelectroniclist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedelectroniclist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedelectroniclist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedelectroniclist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedelectroniclist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedelectroniclist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedelectroniclist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedelectroniclist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedelectroniclist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "electronic");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("emergency_contact")) {
                            final ArrayList<Emergency_contact> clickedemergencylist = handler.getEmergencyContactDetailbyshop_id(shop_name);

                            if (clickedemergencylist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedemergencylist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedemergencylist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedemergencylist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedemergencylist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedemergencylist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedemergencylist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedemergencylist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedemergencylist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedemergencylist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedemergencylist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedemergencylist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedemergencylist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedemergencylist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedemergencylist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "emergency_contact");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("famous_place")) {
                            final ArrayList<Famous_place> clickedfamouslist = handler.getFamousPlaceDetailbyshop_id(shop_name);

                            if (clickedfamouslist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedfamouslist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedfamouslist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedfamouslist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedfamouslist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedfamouslist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedfamouslist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedfamouslist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedfamouslist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedfamouslist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedfamouslist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedfamouslist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedfamouslist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedfamouslist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedfamouslist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "famous_place");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("farmer")) {
                            final ArrayList<Farmer_shop> clickedfarmerlist = handler.getFarmerShopDetailbyshop_id(shop_name);

                            if (clickedfarmerlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedfarmerlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedfarmerlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedfarmerlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedfarmerlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedfarmerlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedfarmerlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedfarmerlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedfarmerlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedfarmerlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedfarmerlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedfarmerlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedfarmerlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedfarmerlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedfarmerlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "farmer");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("gas_station")) {
                            final ArrayList<Gas_station> clickedgas_stationlist = handler.getGasStationDetailbyshop_id(shop_name);
                            if (clickedgas_stationlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedgas_stationlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedgas_stationlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedgas_stationlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedgas_stationlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedgas_stationlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedgas_stationlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedgas_stationlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedgas_stationlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedgas_stationlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedgas_stationlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedgas_stationlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedgas_stationlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedgas_stationlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedgas_stationlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "gas_station");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("hospital")) {
                            final ArrayList<Hospital> clickedhospitallist = handler.getHospitalDetailbyshop_id(shop_name);
                            if (clickedhospitallist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedhospitallist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedhospitallist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedhospitallist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedhospitallist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedhospitallist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedhospitallist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedhospitallist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedhospitallist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedhospitallist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedhospitallist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedhospitallist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedhospitallist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedhospitallist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedhospitallist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "hospital");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("jewellery")) {
                            final ArrayList<Jewellery> clickedjewellerylist = handler.getJewelleryDetailbyshop_id(shop_name);
                            if (clickedjewellerylist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedjewellerylist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedjewellerylist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedjewellerylist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedjewellerylist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedjewellerylist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedjewellerylist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedjewellerylist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedjewellerylist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedjewellerylist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedjewellerylist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedjewellerylist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedjewellerylist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedjewellerylist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedjewellerylist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "jewellery");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("restaurant")) {
                            final ArrayList<Restaurant> clickedrestaurnatlist = handler.getRestaurantDetailbyshop_id(shop_name);
                            if (clickedrestaurnatlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedrestaurnatlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedrestaurnatlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedrestaurnatlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedrestaurnatlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedrestaurnatlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedrestaurnatlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedrestaurnatlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedrestaurnatlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedrestaurnatlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedrestaurnatlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedrestaurnatlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedrestaurnatlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedrestaurnatlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedrestaurnatlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "restaurant");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("hotel")) {
                            final ArrayList<Hotel> clickedhotellist = handler.getHotelDetailbyshop_id(shop_name);
                            if (clickedhotellist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedhotellist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedhotellist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedhotellist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedhotellist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedhotellist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedhotellist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedhotellist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedhotellist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedhotellist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedhotellist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedhotellist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedhotellist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedhotellist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedhotellist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "hotel");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }

                        } else if (table_name.equals("shopping_mall")) {
                            final ArrayList<Shopping_center> clickedshopping_malllist = handler.getShopping_centerDetailbyshop_id(shop_name);
                            if (clickedshopping_malllist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedshopping_malllist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedshopping_malllist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedshopping_malllist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedshopping_malllist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedshopping_malllist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedshopping_malllist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedshopping_malllist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedshopping_malllist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedshopping_malllist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedshopping_malllist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedshopping_malllist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedshopping_malllist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedshopping_malllist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedshopping_malllist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "shopping_mall");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("store")) {
                            final ArrayList<Store> clickedstorelist = handler.getStoreDetailbyshop_id(shop_name);
                            if (clickedstorelist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedstorelist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedstorelist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedstorelist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedstorelist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedstorelist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedstorelist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedstorelist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedstorelist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedstorelist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedstorelist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedstorelist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedstorelist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedstorelist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedstorelist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "store");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("tea_shop")) {
                            final ArrayList<Tea_shop> clickedtea_shoplist = handler.getTeaShopDetailbyshop_id(shop_name);
                            if (clickedtea_shoplist.size() > 0) {

                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedtea_shoplist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedtea_shoplist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedtea_shoplist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedtea_shoplist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedtea_shoplist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedtea_shoplist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedtea_shoplist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedtea_shoplist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedtea_shoplist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedtea_shoplist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedtea_shoplist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedtea_shoplist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedtea_shoplist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedtea_shoplist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "tea_shop");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });

//            if (stickey_ads_list.size() != 0) {
//                imageLoader.DisplayImage(stickey_ads_list.get(0).getAvatar(), stickey_ads);
//            }
        }

        return rootview;
    }

    private void AnimateandSlideShow() {
        imageLoader.DisplayImage(ads_list.get(currentimageindex % ads_list.size()).getAvatar(), slidingimage);

        shop_name = ads_list.get(currentimageindex % ads_list.size()).getShop_id();
        table_name = ads_list.get(currentimageindex % ads_list.size()).getCategory_id();

        currentimageindex++;
    }


    private void AnimateandSlideShow_stickey() {

        imageLoader.DisplayImage(stickey_ads_list.get(currentimageindex_stickey % stickey_ads_list.size()).getAvatar(), stickey_ads);

        currentimageindex_stickey++;
    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;
            serverData_stickey = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://45.120.149.8/my_main_ads.txt");
            HttpGet httpGet_stickey = new HttpGet("http://45.120.149.8/my_stickey_advertisement.txt");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                HttpResponse httpResponse_stickey = httpClient.execute(httpGet_stickey);
                HttpEntity httpEntity_stickey = httpResponse_stickey.getEntity();
                serverData_stickey = EntityUtils.toString(httpEntity_stickey);
                Log.d("response", serverData_stickey);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                bankArrayList = new ArrayList<Ads>();
                JSONObject jsonObject = new JSONObject(serverData);


                JSONArray jsonAdsArray = jsonObject.getJSONArray("Advertisement");

                handler.deleteData("ads_main");
                for (int i = 0; i < jsonAdsArray.length(); i++) {
                    JSONObject jsonObjectNews = jsonAdsArray.getJSONObject(i);
                    String shop_id = jsonObjectNews.getString("shop_id");
                    String category = jsonObjectNews.getString("category");
                    String avatar = "http://45.120.149.8" + jsonObjectNews.getString("avatar_url");
                    String main_ads = jsonObjectNews.getString("main_ads");

                    Ads bank = new Ads();
                    bank.setShop_id(shop_id);
                    bank.setCategory_id(category);
                    bank.setAvatar(avatar);
                    bank.setMain_ads(main_ads);

                    handler.addads(bank);// Inserting into DB
                }

                stickeybankArrayList = new ArrayList<StickeyAds>();
                JSONObject jsonObject_stickey = new JSONObject(serverData_stickey);

                JSONArray jsonAdsArray_stickey = jsonObject_stickey.getJSONArray("Advertisement");

                handler.deleteData("stickey_ads");
                for (int i = 0; i < jsonAdsArray_stickey.length(); i++) {
                    JSONObject jsonObjectNews = jsonAdsArray_stickey.getJSONObject(i);
                    String avatar = "http://45.120.149.8" + jsonObjectNews.getString("avatar_url1");

                    StickeyAds bank = new StickeyAds();
                    bank.setAvatar(avatar);

                    handler.addstickeyads(bank);// Inserting into DB
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

//            if(handler.getTableCount("main") >= 0) {
            ads_list = handler.getMainadsbyCategory();
            stickey_ads_list = handler.getAllstickeyads();

            final android.os.Handler mHandler = new android.os.Handler();
            final Runnable mUpdateResults = new Runnable() {
                public void run() {
                    if (ads_list.size() > 0) {
                        AnimateandSlideShow();
                    }
                    if (stickey_ads_list.size() > 0) {
                        AnimateandSlideShow_stickey();
                    }
                }
            };

            int delay = 500; // delay for 1 sec.
            int period = 1500; // repeat every 4 sec.
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    mHandler.post(mUpdateResults);
                }
            }, delay, period);


            slidingimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ads_list.size() > 0) {
                        if (table_name.equals("bank")) {
                            final ArrayList<Bank> clickedbanklist = handler.getBankDetailbyshop_id(shop_name);

                            if (clickedbanklist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedbanklist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedbanklist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedbanklist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedbanklist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedbanklist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedbanklist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedbanklist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedbanklist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedbanklist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedbanklist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedbanklist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedbanklist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedbanklist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedbanklist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "bank");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("beauty_saloon")) {

                            final ArrayList<Beauty_saloon> clickedbeautylist = handler.getBeautySaloonDetailbyshop_id(shop_name);

                            if (clickedbeautylist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedbeautylist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedbeautylist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedbeautylist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedbeautylist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedbeautylist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedbeautylist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedbeautylist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedbeautylist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedbeautylist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedbeautylist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedbeautylist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedbeautylist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedbeautylist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedbeautylist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "beauty_saloon");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }

                        } else if (table_name.equals("bus")) {

                            final ArrayList<Bus_gate> clickedbuslist = handler.getBusGateDetailbyshop_id(shop_name);
                            if (clickedbuslist.size() > 0) {

                                Intent detail_intent = new Intent(getActivity(), Bus_Gate_Detail.class);
                                detail_intent.putExtra("name", clickedbuslist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedbuslist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedbuslist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedbuslist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedbuslist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedbuslist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedbuslist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedbuslist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedbuslist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedbuslist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedbuslist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedbuslist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedbuslist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedbuslist.get(0).getDesc().toString());
                                detail_intent.putExtra("from_township", clickedbuslist.get(0).getFrom_township().toString());
                                detail_intent.putExtra("to_township", clickedbuslist.get(0).getTo_township().toString());
                                detail_intent.putExtra("departure_time", clickedbuslist.get(0).getDeparture_time().toString());
                                detail_intent.putExtra("category", "bus");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("computer")) {
                            final ArrayList<Computer> clickedcomputerlist = handler.getComputerDetailbyshop_id(shop_name);

                            if (clickedcomputerlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedcomputerlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedcomputerlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedcomputerlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedcomputerlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedcomputerlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedcomputerlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedcomputerlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedcomputerlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedcomputerlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedcomputerlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedcomputerlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedcomputerlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedcomputerlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedcomputerlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "computer");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("cycle")) {
                            final ArrayList<Cycle_shop> clickedcyclelist = handler.getCycleShopDetailbyshop_id(shop_name);

                            if (clickedcyclelist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedcyclelist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedcyclelist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedcyclelist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedcyclelist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedcyclelist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedcyclelist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedcyclelist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedcyclelist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedcyclelist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedcyclelist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedcyclelist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedcyclelist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedcyclelist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedcyclelist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "cycle");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("education")) {

                            final ArrayList<Education> clickededucationlist = handler.getEducationDetailbyshop_id(shop_name);

                            if (clickededucationlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickededucationlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickededucationlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickededucationlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickededucationlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickededucationlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickededucationlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickededucationlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickededucationlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickededucationlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickededucationlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickededucationlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickededucationlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickededucationlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickededucationlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "education");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("electronic")) {
                            final ArrayList<Electronic_store> clickedelectroniclist = handler.getElectronicShopDetailbyshop_id(shop_name);

                            if (clickedelectroniclist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedelectroniclist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedelectroniclist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedelectroniclist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedelectroniclist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedelectroniclist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedelectroniclist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedelectroniclist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedelectroniclist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedelectroniclist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedelectroniclist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedelectroniclist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedelectroniclist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedelectroniclist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedelectroniclist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "electronic");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("emergency_contact")) {
                            final ArrayList<Emergency_contact> clickedemergencylist = handler.getEmergencyContactDetailbyshop_id(shop_name);

                            if (clickedemergencylist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedemergencylist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedemergencylist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedemergencylist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedemergencylist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedemergencylist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedemergencylist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedemergencylist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedemergencylist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedemergencylist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedemergencylist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedemergencylist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedemergencylist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedemergencylist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedemergencylist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "emergency_contact");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("famous_place")) {
                            final ArrayList<Famous_place> clickedfamouslist = handler.getFamousPlaceDetailbyshop_id(shop_name);

                            if (clickedfamouslist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedfamouslist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedfamouslist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedfamouslist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedfamouslist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedfamouslist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedfamouslist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedfamouslist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedfamouslist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedfamouslist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedfamouslist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedfamouslist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedfamouslist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedfamouslist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedfamouslist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "famous_place");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("farmer")) {
                            final ArrayList<Farmer_shop> clickedfarmerlist = handler.getFarmerShopDetailbyshop_id(shop_name);

                            if (clickedfarmerlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedfarmerlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedfarmerlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedfarmerlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedfarmerlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedfarmerlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedfarmerlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedfarmerlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedfarmerlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedfarmerlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedfarmerlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedfarmerlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedfarmerlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedfarmerlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedfarmerlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "farmer");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("gas_station")) {
                            final ArrayList<Gas_station> clickedgas_stationlist = handler.getGasStationDetailbyshop_id(shop_name);
                            if (clickedgas_stationlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedgas_stationlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedgas_stationlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedgas_stationlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedgas_stationlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedgas_stationlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedgas_stationlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedgas_stationlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedgas_stationlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedgas_stationlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedgas_stationlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedgas_stationlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedgas_stationlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedgas_stationlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedgas_stationlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "gas_station");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("hospital")) {
                            final ArrayList<Hospital> clickedhospitallist = handler.getHospitalDetailbyshop_id(shop_name);
                            if (clickedhospitallist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedhospitallist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedhospitallist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedhospitallist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedhospitallist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedhospitallist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedhospitallist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedhospitallist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedhospitallist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedhospitallist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedhospitallist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedhospitallist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedhospitallist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedhospitallist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedhospitallist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "hospital");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("jewellery")) {
                            final ArrayList<Jewellery> clickedjewellerylist = handler.getJewelleryDetailbyshop_id(shop_name);
                            if (clickedjewellerylist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedjewellerylist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedjewellerylist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedjewellerylist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedjewellerylist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedjewellerylist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedjewellerylist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedjewellerylist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedjewellerylist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedjewellerylist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedjewellerylist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedjewellerylist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedjewellerylist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedjewellerylist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedjewellerylist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "jewellery");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("restaurant")) {
                            final ArrayList<Restaurant> clickedrestaurnatlist = handler.getRestaurantDetailbyshop_id(shop_name);
                            if (clickedrestaurnatlist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedrestaurnatlist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedrestaurnatlist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedrestaurnatlist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedrestaurnatlist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedrestaurnatlist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedrestaurnatlist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedrestaurnatlist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedrestaurnatlist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedrestaurnatlist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedrestaurnatlist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedrestaurnatlist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedrestaurnatlist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedrestaurnatlist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedrestaurnatlist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "restaurant");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("hotel")) {
                            final ArrayList<Hotel> clickedhotellist = handler.getHotelDetailbyshop_id(shop_name);
                            if (clickedhotellist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedhotellist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedhotellist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedhotellist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedhotellist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedhotellist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedhotellist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedhotellist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedhotellist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedhotellist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedhotellist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedhotellist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedhotellist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedhotellist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedhotellist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "hotel");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }

                        } else if (table_name.equals("shopping_mall")) {
                            final ArrayList<Shopping_center> clickedshopping_malllist = handler.getShopping_centerDetailbyshop_id(shop_name);
                            if (clickedshopping_malllist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedshopping_malllist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedshopping_malllist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedshopping_malllist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedshopping_malllist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedshopping_malllist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedshopping_malllist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedshopping_malllist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedshopping_malllist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedshopping_malllist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedshopping_malllist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedshopping_malllist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedshopping_malllist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedshopping_malllist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedshopping_malllist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "shopping_mall");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("store")) {
                            final ArrayList<Store> clickedstorelist = handler.getStoreDetailbyshop_id(shop_name);
                            if (clickedstorelist.size() > 0) {
                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedstorelist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedstorelist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedstorelist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedstorelist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedstorelist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedstorelist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedstorelist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedstorelist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedstorelist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedstorelist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedstorelist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedstorelist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedstorelist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedstorelist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "store");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (table_name.equals("tea_shop")) {
                            final ArrayList<Tea_shop> clickedtea_shoplist = handler.getTeaShopDetailbyshop_id(shop_name);
                            if (clickedtea_shoplist.size() > 0) {

                                Intent detail_intent = new Intent(getActivity(), Detail.class);
                                detail_intent.putExtra("name", clickedtea_shoplist.get(0).getName().toString());
                                detail_intent.putExtra("address", clickedtea_shoplist.get(0).getAddress().toString());
                                detail_intent.putExtra("phone", clickedtea_shoplist.get(0).getPhone().toString());
                                detail_intent.putExtra("phone1", clickedtea_shoplist.get(0).getPhone1().toString());
                                detail_intent.putExtra("phone2", clickedtea_shoplist.get(0).getPhone2().toString());
                                detail_intent.putExtra("website", clickedtea_shoplist.get(0).getWebsite().toString());
                                detail_intent.putExtra("latitude", clickedtea_shoplist.get(0).getLatitude().toString());
                                detail_intent.putExtra("longitude", clickedtea_shoplist.get(0).getLongitude().toString());
                                detail_intent.putExtra("avatar", clickedtea_shoplist.get(0).getAvatar().toString());
                                detail_intent.putExtra("avatar1", clickedtea_shoplist.get(0).getAvatar1().toString());
                                detail_intent.putExtra("avatar2", clickedtea_shoplist.get(0).getAvatar2().toString());
                                detail_intent.putExtra("avatar3", clickedtea_shoplist.get(0).getAvatar3().toString());
                                detail_intent.putExtra("avatar4", clickedtea_shoplist.get(0).getAvatar4().toString());
                                detail_intent.putExtra("description", clickedtea_shoplist.get(0).getDesc().toString());
                                detail_intent.putExtra("category", "tea_shop");
                                startActivity(detail_intent);
                            } else {
                                Toast.makeText(getActivity(), "Please update your data first.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        currentimageindex = 0;
        currentimageindex_stickey = 0;
    }
}

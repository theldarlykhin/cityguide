package com.hnttechs.www.cityguide;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dell on 7/15/15.
 */
public class GridViewAdapter extends BaseAdapter {

    private Context context;
    private final String[] gridValues;
    private final int[] gridImages;

    public GridViewAdapter(Context context, String[] gridValues, int[] gridImages) {
        this.context        = context;
        this.gridValues     = gridValues;
        this.gridImages     = gridImages;
    }

    @Override
    public int getCount() { return gridValues.length; }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate( R.layout.homescreen_grid_item , null);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder = new ViewHolder();
        viewHolder.img_icon = (ImageView) convertView
                .findViewById(R.id.img_homescreen_icon);
        viewHolder.txt_title = (TextView)convertView.findViewById(R.id.txt_homescreen_title);
        convertView.setTag(viewHolder);

        Typeface tf_zawgyi = Typeface.createFromAsset(context.getAssets(), "fonts/zawgyione.ttf");
        viewHolder.txt_title.setTypeface(tf_zawgyi);
        viewHolder.txt_title.setText(gridValues[position].toString());
        viewHolder.img_icon.setImageResource(gridImages[position]);



        return convertView;
    }


    static class ViewHolder
    {
        TextView txt_title;
        ImageView img_icon;
    }

}

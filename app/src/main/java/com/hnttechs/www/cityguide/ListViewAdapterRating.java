package com.hnttechs.www.cityguide;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapterRating extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;

    ArrayList<Rating> bank_listData;

    public ListViewAdapterRating(Context context, ArrayList<Rating> listData) {
        this.context = context;
        bank_listData = listData;

    }

    @Override
    public int getCount() {
        return bank_listData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.rating_list_item, parent, false);
        }


        final TextView txt_rating_user_name = (TextView) convertView.findViewById(R.id.txt_rating_user_name);
        final RatingBar rt_rating_bar = (RatingBar) convertView.findViewById(R.id.rt_rating_bar);

        Rating bank = bank_listData.get(position);

        String name = bank.getUser_name();
        String rate_number = bank.getRate_number();

        txt_rating_user_name.setText(name);

        Float fl;

        if(rate_number.equals("1.0")) {
            fl = 1.0f;
            rt_rating_bar.setRating(fl);
        } else if (rate_number.equals("2.0")) {
            fl = 2.0f;
            rt_rating_bar.setRating(fl);
        }else if (rate_number.equals("3.0")) {
            fl = 3.0f;
            rt_rating_bar.setRating(fl);
        }else if (rate_number.equals("4.0")) {
            fl = 4.0f;
            rt_rating_bar.setRating(fl);
        }else if (rate_number.equals("5.0")) {
            fl = 5.0f;
            rt_rating_bar.setRating(fl);
        }

        return convertView;
    }
}
package com.hnttechs.www.cityguide;

/**
 * Created by dell on 8/20/16.
 */
public class StickeyAds {
    private int id;
    private String avatar;


    public StickeyAds() {
    }

    public StickeyAds(String avatar) {
        this.avatar = avatar;
    }

    public StickeyAds(int id, String avatar) {
        this.id = id;
        this.avatar = avatar;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}

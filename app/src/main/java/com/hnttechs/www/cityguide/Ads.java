package com.hnttechs.www.cityguide;

/**
 * Created by dell on 8/20/16.
 */
public class Ads {
    private int id;
    private String shop_id;
    private String category_id;
    private String avatar;
    private String main_ads;


    public Ads() {
    }

    public Ads(String shop_id, String category_id, String avatar, String main_ads) {
        this.shop_id = shop_id;
        this.category_id = category_id;
        this.avatar = avatar;
        this.main_ads= main_ads;
    }

    public Ads(int id, String shop_id, String category_id, String avatar, String main_ads) {
        this.id = id;
        this.shop_id = shop_id;
        this.category_id = category_id;
        this.avatar = avatar;
        this.main_ads= main_ads;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id= shop_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id= category_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMainAds() {
        return main_ads;
    }

    public void setMain_ads(String main_ads) {
        this.main_ads = main_ads;
    }
}

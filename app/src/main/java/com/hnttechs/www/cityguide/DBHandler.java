package com.hnttechs.www.cityguide;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 8/20/16.
 */
public class DBHandler extends SQLiteOpenHelper {
    private static final int DB_VERSION = 45;
    private static final String DB_NAME = "CityGuideDatabase.db";
    private static final String TABLE_BANK = "bank_table";
    private static final String TABLE_ADS = "ads_table";
    private static final String TABLE_STICKEY_ADS = "stickey_ads_table";
    private static final String TABLE_COMPUTER = "computer_table";
    private static final String TABLE_HOTEL = "hotel_table";
    private static final String TABLE_EDUCATION = "education_table";
    private static final String TABLE_HOSPITAL = "hospital_table";
    private static final String TABLE_STORE = "store_table";
    private static final String TABLE_FAMOUS_PLACE = "famous_place_table";
    private static final String TABLE_GAS_STATION = "gas_station_table";
    private static final String TABLE_JEWELLERY = "jewellery_table";
    private static final String TABLE_SHOPPING_CENTER = "shopping_center_table";
    private static final String TABLE_EMERGENCY_CONTACT = "emergency_contact_table";
    private static final String TABLE_FARMER_SHOP = "farmer_shop_table";
    private static final String TABLE_BEAUTY_SALOON = "beauty_saloon_table";
    private static final String TABLE_BUS_GATE = "bus_gate_table";
    private static final String TABLE_ELECTRONIC_SHOP = "electronic_shop_table";
    private static final String TABLE_TEA_SHOP = "tea_shop_table";
    private static final String TABLE_CYCLE_SHOP = "cycle_shop_table";
    private static final String TABLE_RESTAURANT = "restaurant_table";
    private static final String TABLE_RATE = "rate_table";
    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "_name";
    private static final String KEY_ADDRESS = "_address";
    private static final String KEY_PHONE = "_phone";
    private static final String KEY_PHONE1 = "_phone1";
    private static final String KEY_PHONE2 = "_phone2";
    private static final String KEY_WEBSITE = "_website";
    private static final String KEY_LATITUDE = "_latitude";
    private static final String KEY_LONGITUDE = "_longitude";
    private static final String KEY_FROM_TOWNSHIP = "_fromtownship";
    private static final String KEY_TO_TOWNSHIP = "_totownship";
    private static final String KEY_DEPARTURE_TIME = "_departuretime";
    private static final String KEY_HOTELTYPE  = "_hoteltype";
    private static final String KEY_COMPUTERTYPE  = "_computertype";
    private static final String KEY_SHOP_ID  = "_shop_id";
    private static final String KEY_CATEGORY_ID  = "_category_id";
    private static final String KEY_AVATAR = "_avatar";
    private static final String KEY_AVATAR1= "_avatar1";
    private static final String KEY_AVATAR2= "_avatar2";
    private static final String KEY_AVATAR3= "_avatar3";
    private static final String KEY_AVATAR4= "_avatar4";
    private static final String KEY_DESC= "_desc";
    private static final String KEY_MAIN_ADS= "_main_ads";
    private static final String KEY_USERNAME= "_username";
    private static final String KEY_RATENAME= "_ratename";
    private static final String KEY_RATENUMBER= "_ratenumber";
    private static final String KEY_RATECATEGORY= "_ratecategory";
    private static String QUERY;

    static List<String> to_region_list = new ArrayList<String>();
    static List<String> from_region_list = new ArrayList<String>();

    String CREATE_BANK_TABLE = "CREATE TABLE " + TABLE_BANK + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT," + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String DROP_BANK_TABLE = "DROP TABLE IF EXISTS " + TABLE_BANK;


    String CREATE_COMPUTER_TABLE = "CREATE TABLE " + TABLE_COMPUTER + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_COMPUTERTYPE+ " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String DROP_COMPUTER_TABLE = "DROP TABLE IF EXISTS " + TABLE_COMPUTER;


    String CREATE_HOTEL_TABLE = "CREATE TABLE " + TABLE_HOTEL + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," + KEY_HOTELTYPE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String DROP_HOTEL_TABLE = "DROP TABLE IF EXISTS " + TABLE_HOTEL;


    String CREATE_EDUCATION_TABLE = "CREATE TABLE " + TABLE_EDUCATION + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String CREATE_HOSPITAL_TABLE = "CREATE TABLE " + TABLE_HOSPITAL + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String CREATE_STORE_TABLE = "CREATE TABLE " + TABLE_STORE + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String CREATE_FAMOUS_PLACE_TABLE = "CREATE TABLE " + TABLE_FAMOUS_PLACE + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_GAS_STATION_TABLE = "CREATE TABLE " + TABLE_GAS_STATION + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_JEWELLERY_TABLE = "CREATE TABLE " + TABLE_JEWELLERY + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_SHOPPING_CENTER_TABLE = "CREATE TABLE " + TABLE_SHOPPING_CENTER + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE
            + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_EMERGENCY_CONTACT_TABLE = "CREATE TABLE " + TABLE_EMERGENCY_CONTACT + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_FARMER_SHOP_TABLE = "CREATE TABLE " + TABLE_FARMER_SHOP + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_BEAUTY_SALOON_TABLE = "CREATE TABLE " + TABLE_BEAUTY_SALOON + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE  + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_BUS_GATE_TABLE = "CREATE TABLE " + TABLE_BUS_GATE + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE + " TEXT," + KEY_FROM_TOWNSHIP + " TEXT," + KEY_TO_TOWNSHIP +
            " TEXT," + KEY_DEPARTURE_TIME  + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_ELECTRONIC_SHOP_TABLE = "CREATE TABLE " + TABLE_ELECTRONIC_SHOP + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE  + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String CREATE_TEA_SHOP_TABLE = "CREATE TABLE " + TABLE_TEA_SHOP + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE  + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String CREATE_CYCLE_SHOP_TABLE = "CREATE TABLE " + TABLE_CYCLE_SHOP + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE  + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";
    String CREATE_RESTAURANT_TABLE = "CREATE TABLE " + TABLE_RESTAURANT + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_ADDRESS + " TEXT," + KEY_PHONE + " TEXT," +KEY_PHONE1+ " TEXT," +KEY_PHONE2+ " TEXT,"  + KEY_WEBSITE + " TEXT," + KEY_LATITUDE
            + " TEXT," + KEY_LONGITUDE  + " TEXT," +KEY_AVATAR+ " TEXT," +KEY_AVATAR1+ " TEXT," +KEY_AVATAR2+ " TEXT,"
            +KEY_AVATAR3+ " TEXT," +KEY_AVATAR4+ " TEXT," +KEY_DESC+ " TEXT)";

    String CREATE_ADS_TABLE = "CREATE TABLE " +TABLE_ADS + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SHOP_ID
            + " TEXT," +KEY_CATEGORY_ID+ " TEXT," + KEY_AVATAR + " TEXT," + KEY_MAIN_ADS+ " TEXT)";

    String CREATE_STICKEY_ADS_TABLE = "CREATE TABLE " +TABLE_STICKEY_ADS+ " (" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_AVATAR + " TEXT)";

    String CREATE_RATE_TABLE = "CREATE TABLE " +TABLE_RATE+ " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USERNAME
            + " TEXT," +KEY_RATENAME+ " TEXT," + KEY_RATENUMBER+ " TEXT," +KEY_RATECATEGORY+" TEXT)";

    String DROP_RATE_TALBE = "DROP TABLE IF EXISTS " + TABLE_RATE;

    String DROP_EDUCATION_TABLE = "DROP TABLE IF EXISTS " + TABLE_EDUCATION;
    String DROP_HOSPITAL_TABLE = "DROP TABLE IF EXISTS " + TABLE_HOSPITAL;
    String DROP_STORE_TABLE = "DROP TABLE IF EXISTS " + TABLE_STORE;
    String DROP_FAMOUS_PLACE_TABLE = "DROP TABLE IF EXISTS " + TABLE_FAMOUS_PLACE;
    String DROP_GAS_STATION_TABLE = "DROP TABLE IF EXISTS " + TABLE_GAS_STATION;
    String DROP_JEWELLERY_TABLE = "DROP TABLE IF EXISTS " + TABLE_JEWELLERY;
    String DROP_SHOPPING_CENTER_TABLE = "DROP TABLE IF EXISTS " + TABLE_SHOPPING_CENTER;
    String DROP_EMERGENCY_CONTACT_TABLE = "DROP TABLE IF EXISTS " + TABLE_EMERGENCY_CONTACT;
    String DROP_FARMER_SHOP_TABLE = "DROP TABLE IF EXISTS " + TABLE_FARMER_SHOP;
    String DROP_BEAUTY_SALOON_TABLE = "DROP TABLE IF EXISTS " + TABLE_BEAUTY_SALOON;
    String DROP_BUS_GATE_TABLE = "DROP TABLE IF EXISTS " + TABLE_BUS_GATE;
    String DROP_ELECTRONIC_SHOP_TABLE = "DROP TABLE IF EXISTS " + TABLE_ELECTRONIC_SHOP;
    String DROP_TEA_SHOP_TABLE = "DROP TABLE IF EXISTS " + TABLE_TEA_SHOP;
    String DROP_CYCLE_SHOP_TABLE = "DROP TABLE IF EXISTS " + TABLE_CYCLE_SHOP;
    String DROP_RESTAURANT_TABLE = "DROP TABLE IF EXISTS " + TABLE_RESTAURANT;
    String DROP_ADS_TABLE = "DROP TABLE IF EXISTS " + TABLE_ADS;
    String DROP_STICKEY_ADS_TABLE = "DROP TABLE IF EXISTS " + TABLE_STICKEY_ADS;


    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BANK_TABLE);
        db.execSQL(CREATE_COMPUTER_TABLE);
        db.execSQL(CREATE_HOTEL_TABLE);
        db.execSQL(CREATE_EDUCATION_TABLE);
        db.execSQL(CREATE_HOSPITAL_TABLE);
        db.execSQL(CREATE_STORE_TABLE);
        db.execSQL(CREATE_FAMOUS_PLACE_TABLE);
        db.execSQL(CREATE_GAS_STATION_TABLE);
        db.execSQL(CREATE_JEWELLERY_TABLE);
        db.execSQL(CREATE_SHOPPING_CENTER_TABLE);
        db.execSQL(CREATE_EMERGENCY_CONTACT_TABLE);
        db.execSQL(CREATE_FARMER_SHOP_TABLE);
        db.execSQL(CREATE_BEAUTY_SALOON_TABLE);
        db.execSQL(CREATE_BUS_GATE_TABLE);
        db.execSQL(CREATE_ELECTRONIC_SHOP_TABLE);
        db.execSQL(CREATE_TEA_SHOP_TABLE);
        db.execSQL(CREATE_CYCLE_SHOP_TABLE);
        db.execSQL(CREATE_RESTAURANT_TABLE);
        db.execSQL(CREATE_ADS_TABLE);
        db.execSQL(CREATE_STICKEY_ADS_TABLE);
        db.execSQL(CREATE_RATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_BANK_TABLE);
        db.execSQL(DROP_COMPUTER_TABLE);
        db.execSQL(DROP_HOTEL_TABLE);
        db.execSQL(DROP_EDUCATION_TABLE);
        db.execSQL(DROP_HOSPITAL_TABLE);
        db.execSQL(DROP_STORE_TABLE);
        db.execSQL(DROP_FAMOUS_PLACE_TABLE);
        db.execSQL(DROP_GAS_STATION_TABLE);
        db.execSQL(DROP_JEWELLERY_TABLE);
        db.execSQL(DROP_SHOPPING_CENTER_TABLE);
        db.execSQL(DROP_EMERGENCY_CONTACT_TABLE);
        db.execSQL(DROP_FARMER_SHOP_TABLE);
        db.execSQL(DROP_BEAUTY_SALOON_TABLE);
        db.execSQL(DROP_BUS_GATE_TABLE);
        db.execSQL(DROP_ELECTRONIC_SHOP_TABLE);
        db.execSQL(DROP_TEA_SHOP_TABLE);
        db.execSQL(DROP_CYCLE_SHOP_TABLE);
        db.execSQL(DROP_RESTAURANT_TABLE);
        db.execSQL(DROP_ADS_TABLE);
        db.execSQL(DROP_STICKEY_ADS_TABLE);
        db.execSQL(DROP_RATE_TALBE);

        onCreate(db);
    }

    public void addbank(Bank bank) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, bank.getName());
            values.put(KEY_ADDRESS, bank.getAddress());
            values.put(KEY_PHONE, bank.getPhone());
            values.put(KEY_PHONE1, bank.getPhone1());
            values.put(KEY_PHONE2, bank.getPhone2());
            values.put(KEY_WEBSITE, bank.getWebsite());
            values.put(KEY_LATITUDE, bank.getLatitude());
            values.put(KEY_LONGITUDE, bank.getLongitude());
            values.put(KEY_AVATAR, bank.getAvatar());
            values.put(KEY_AVATAR1, bank.getAvatar1());
            values.put(KEY_AVATAR2, bank.getAvatar2());
            values.put(KEY_AVATAR3, bank.getAvatar3());
            values.put(KEY_AVATAR4, bank.getAvatar4());
            values.put(KEY_DESC, bank.getDesc());
            db.insert(TABLE_BANK, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }
    public void addrate(Rating bank) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_USERNAME, bank.getUser_name());
            values.put(KEY_RATENAME, bank.getRate_name());
            values.put(KEY_RATENUMBER, bank.getRate_number());
            values.put(KEY_RATECATEGORY, bank.getRate_category());
            db.insert(TABLE_RATE, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addcomputer(Computer computer) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, computer.getName());
            values.put(KEY_ADDRESS, computer.getAddress());
            values.put(KEY_PHONE, computer.getPhone());
            values.put(KEY_PHONE1, computer.getPhone1());
            values.put(KEY_PHONE2, computer.getPhone2());
            values.put(KEY_WEBSITE, computer.getWebsite());
            values.put(KEY_LATITUDE, computer.getLatitude());
            values.put(KEY_LONGITUDE, computer.getLongitude());
            values.put(KEY_COMPUTERTYPE, computer.getCategory_type());
            values.put(KEY_AVATAR, computer.getAvatar());
            values.put(KEY_AVATAR1, computer.getAvatar1());
            values.put(KEY_AVATAR2, computer.getAvatar2());
            values.put(KEY_AVATAR3, computer.getAvatar3());
            values.put(KEY_AVATAR4, computer.getAvatar4());
            values.put(KEY_DESC, computer.getDesc());
            db.insert(TABLE_COMPUTER, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addhotel(Hotel hotel) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, hotel.getName());
            values.put(KEY_ADDRESS, hotel.getAddress());
            values.put(KEY_PHONE, hotel.getPhone());
            values.put(KEY_PHONE1, hotel.getPhone1());
            values.put(KEY_PHONE2, hotel.getPhone2());
            values.put(KEY_WEBSITE, hotel.getWebsite());
            values.put(KEY_LATITUDE, hotel.getLatitude());
            values.put(KEY_LONGITUDE, hotel.getLongitude());
            values.put(KEY_HOTELTYPE, hotel.getHoteltype());
            values.put(KEY_AVATAR, hotel.getAvatar());
            values.put(KEY_AVATAR1, hotel.getAvatar1());
            values.put(KEY_AVATAR2, hotel.getAvatar2());
            values.put(KEY_AVATAR3, hotel.getAvatar3());
            values.put(KEY_AVATAR4, hotel.getAvatar4());
            values.put(KEY_DESC, hotel.getDesc());
            db.insert(TABLE_HOTEL, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addeducation(Education education) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, education.getName());
            values.put(KEY_ADDRESS, education.getAddress());
            values.put(KEY_PHONE, education.getPhone());
            values.put(KEY_PHONE1, education.getPhone1());
            values.put(KEY_PHONE2, education.getPhone2());
            values.put(KEY_WEBSITE, education.getWebsite());
            values.put(KEY_LATITUDE, education.getLatitude());
            values.put(KEY_LONGITUDE, education.getLongitude());
            values.put(KEY_AVATAR, education.getAvatar());
            values.put(KEY_AVATAR1, education.getAvatar1());
            values.put(KEY_AVATAR2, education.getAvatar2());
            values.put(KEY_AVATAR3, education.getAvatar3());
            values.put(KEY_AVATAR4, education.getAvatar4());
            values.put(KEY_DESC, education.getDesc());
            db.insert(TABLE_EDUCATION, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addhospital(Hospital hospital) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, hospital.getName());
            values.put(KEY_ADDRESS, hospital.getAddress());
            values.put(KEY_PHONE, hospital.getPhone());
            values.put(KEY_PHONE1, hospital.getPhone1());
            values.put(KEY_PHONE2, hospital.getPhone2());
            values.put(KEY_WEBSITE, hospital.getWebsite());
            values.put(KEY_LATITUDE, hospital.getLatitude());
            values.put(KEY_LONGITUDE, hospital.getLongitude());
            values.put(KEY_AVATAR, hospital.getAvatar());
            values.put(KEY_AVATAR1, hospital.getAvatar1());
            values.put(KEY_AVATAR2, hospital.getAvatar2());
            values.put(KEY_AVATAR3, hospital.getAvatar3());
            values.put(KEY_AVATAR4, hospital.getAvatar4());
            values.put(KEY_DESC, hospital.getDesc());
            db.insert(TABLE_HOSPITAL, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addstore(Store store) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, store.getName());
            values.put(KEY_ADDRESS, store.getAddress());
            values.put(KEY_PHONE, store.getPhone());
            values.put(KEY_PHONE1, store.getPhone1());
            values.put(KEY_PHONE2, store.getPhone2());
            values.put(KEY_WEBSITE, store.getWebsite());
            values.put(KEY_LATITUDE, store.getLatitude());
            values.put(KEY_LONGITUDE, store.getLongitude());
            values.put(KEY_AVATAR, store.getAvatar());
            values.put(KEY_AVATAR1, store.getAvatar1());
            values.put(KEY_AVATAR2, store.getAvatar2());
            values.put(KEY_AVATAR3, store.getAvatar3());
            values.put(KEY_AVATAR4, store.getAvatar4());
            values.put(KEY_DESC, store.getDesc());
            db.insert(TABLE_STORE, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addfamous_place(Famous_place famous_place) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, famous_place.getName());
            values.put(KEY_ADDRESS, famous_place.getAddress());
            values.put(KEY_PHONE, famous_place.getPhone());
            values.put(KEY_PHONE1, famous_place.getPhone1());
            values.put(KEY_PHONE2, famous_place.getPhone2());
            values.put(KEY_WEBSITE, famous_place.getWebsite());
            values.put(KEY_LATITUDE, famous_place.getLatitude());
            values.put(KEY_LONGITUDE, famous_place.getLongitude());
            values.put(KEY_AVATAR, famous_place.getAvatar());
            values.put(KEY_AVATAR1, famous_place.getAvatar1());
            values.put(KEY_AVATAR2, famous_place.getAvatar2());
            values.put(KEY_AVATAR3, famous_place.getAvatar3());
            values.put(KEY_AVATAR4, famous_place.getAvatar4());
            values.put(KEY_DESC, famous_place.getDesc());
            db.insert(TABLE_FAMOUS_PLACE, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addgas_station(Gas_station gas_station) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, gas_station.getName());
            values.put(KEY_ADDRESS, gas_station.getAddress());
            values.put(KEY_PHONE, gas_station.getPhone());
            values.put(KEY_PHONE1, gas_station.getPhone1());
            values.put(KEY_PHONE2, gas_station.getPhone2());
            values.put(KEY_WEBSITE, gas_station.getWebsite());
            values.put(KEY_LATITUDE, gas_station.getLatitude());
            values.put(KEY_LONGITUDE, gas_station.getLongitude());
            values.put(KEY_AVATAR, gas_station.getAvatar());
            values.put(KEY_AVATAR1, gas_station.getAvatar1());
            values.put(KEY_AVATAR2, gas_station.getAvatar2());
            values.put(KEY_AVATAR3, gas_station.getAvatar3());
            values.put(KEY_AVATAR4, gas_station.getAvatar4());
            values.put(KEY_DESC, gas_station.getDesc());
            db.insert(TABLE_GAS_STATION, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addjewellery(Jewellery jewellery) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, jewellery.getName());
            values.put(KEY_ADDRESS, jewellery.getAddress());
            values.put(KEY_PHONE, jewellery.getPhone());
            values.put(KEY_PHONE1, jewellery.getPhone1());
            values.put(KEY_PHONE2, jewellery.getPhone2());
            values.put(KEY_WEBSITE, jewellery.getWebsite());
            values.put(KEY_LATITUDE, jewellery.getLatitude());
            values.put(KEY_LONGITUDE, jewellery.getLongitude());
            values.put(KEY_AVATAR, jewellery.getAvatar());
            values.put(KEY_AVATAR1, jewellery.getAvatar1());
            values.put(KEY_AVATAR2, jewellery.getAvatar2());
            values.put(KEY_AVATAR3, jewellery.getAvatar3());
            values.put(KEY_AVATAR4, jewellery.getAvatar4());
            values.put(KEY_DESC, jewellery.getDesc());
            db.insert(TABLE_JEWELLERY, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addshopping_centerr(Shopping_center shopping_center) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, shopping_center.getName());
            values.put(KEY_ADDRESS, shopping_center.getAddress());
            values.put(KEY_PHONE, shopping_center.getPhone());
            values.put(KEY_PHONE1, shopping_center.getPhone1());
            values.put(KEY_PHONE2, shopping_center.getPhone2());
            values.put(KEY_WEBSITE, shopping_center.getWebsite());
            values.put(KEY_LATITUDE, shopping_center.getLatitude());
            values.put(KEY_LONGITUDE, shopping_center.getLongitude());
            values.put(KEY_AVATAR, shopping_center.getAvatar());
            values.put(KEY_AVATAR1, shopping_center.getAvatar1());
            values.put(KEY_AVATAR2, shopping_center.getAvatar2());
            values.put(KEY_AVATAR3, shopping_center.getAvatar3());
            values.put(KEY_AVATAR4, shopping_center.getAvatar4());
            values.put(KEY_DESC, shopping_center.getDesc());
            db.insert(TABLE_SHOPPING_CENTER, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addemergency_contact(Emergency_contact emergency_contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, emergency_contact.getName());
            values.put(KEY_ADDRESS, emergency_contact.getAddress());
            values.put(KEY_PHONE, emergency_contact.getPhone());
            values.put(KEY_PHONE1, emergency_contact.getPhone1());
            values.put(KEY_PHONE2, emergency_contact.getPhone2());
            values.put(KEY_WEBSITE, emergency_contact.getWebsite());
            values.put(KEY_LATITUDE, emergency_contact.getLatitude());
            values.put(KEY_LONGITUDE, emergency_contact.getLongitude());
            values.put(KEY_AVATAR, emergency_contact.getAvatar());
            values.put(KEY_AVATAR1, emergency_contact.getAvatar1());
            values.put(KEY_AVATAR2, emergency_contact.getAvatar2());
            values.put(KEY_AVATAR3, emergency_contact.getAvatar3());
            values.put(KEY_AVATAR4, emergency_contact.getAvatar4());
            values.put(KEY_DESC, emergency_contact.getDesc());
            db.insert(TABLE_EMERGENCY_CONTACT, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addfarmer_shop(Farmer_shop farmer_shop) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, farmer_shop.getName());
            values.put(KEY_ADDRESS, farmer_shop.getAddress());
            values.put(KEY_PHONE, farmer_shop.getPhone());
            values.put(KEY_PHONE1, farmer_shop.getPhone1());
            values.put(KEY_PHONE2, farmer_shop.getPhone2());
            values.put(KEY_WEBSITE, farmer_shop.getWebsite());
            values.put(KEY_LATITUDE, farmer_shop.getLatitude());
            values.put(KEY_LONGITUDE, farmer_shop.getLongitude());
            values.put(KEY_AVATAR, farmer_shop.getAvatar());
            values.put(KEY_AVATAR1, farmer_shop.getAvatar1());
            values.put(KEY_AVATAR2, farmer_shop.getAvatar2());
            values.put(KEY_AVATAR3, farmer_shop.getAvatar3());
            values.put(KEY_AVATAR4, farmer_shop.getAvatar4());
            values.put(KEY_DESC, farmer_shop.getDesc());
            db.insert(TABLE_FARMER_SHOP, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addbeauty_saloon(Beauty_saloon beauty_saloon) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, beauty_saloon.getName());
            values.put(KEY_ADDRESS, beauty_saloon.getAddress());
            values.put(KEY_PHONE, beauty_saloon.getPhone());
            values.put(KEY_PHONE1, beauty_saloon.getPhone1());
            values.put(KEY_PHONE2, beauty_saloon.getPhone2());
            values.put(KEY_WEBSITE, beauty_saloon.getWebsite());
            values.put(KEY_LATITUDE, beauty_saloon.getLatitude());
            values.put(KEY_LONGITUDE, beauty_saloon.getLongitude());
            values.put(KEY_AVATAR, beauty_saloon.getAvatar());
            values.put(KEY_AVATAR1, beauty_saloon.getAvatar1());
            values.put(KEY_AVATAR2, beauty_saloon.getAvatar2());
            values.put(KEY_AVATAR3, beauty_saloon.getAvatar3());
            values.put(KEY_AVATAR4, beauty_saloon.getAvatar4());
            values.put(KEY_DESC, beauty_saloon.getDesc());
            db.insert(TABLE_BEAUTY_SALOON, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addbus_gate(Bus_gate bus_gate) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, bus_gate.getName());
            values.put(KEY_ADDRESS, bus_gate.getAddress());
            values.put(KEY_PHONE, bus_gate.getPhone());
            values.put(KEY_PHONE1, bus_gate.getPhone1());
            values.put(KEY_PHONE2, bus_gate.getPhone2());
            values.put(KEY_WEBSITE, bus_gate.getWebsite());
            values.put(KEY_LATITUDE, bus_gate.getLatitude());
            values.put(KEY_LONGITUDE, bus_gate.getLongitude());
            values.put(KEY_FROM_TOWNSHIP, bus_gate.getFrom_township());
            values.put(KEY_TO_TOWNSHIP, bus_gate.getTo_township());
            values.put(KEY_DEPARTURE_TIME, bus_gate.getDeparture_time());
            values.put(KEY_AVATAR, bus_gate.getAvatar());
            values.put(KEY_AVATAR1, bus_gate.getAvatar1());
            values.put(KEY_AVATAR2, bus_gate.getAvatar2());
            values.put(KEY_AVATAR3, bus_gate.getAvatar3());
            values.put(KEY_AVATAR4, bus_gate.getAvatar4());
            values.put(KEY_DESC, bus_gate.getDesc());
            db.insert(TABLE_BUS_GATE, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addelectronic_shop(Electronic_store electronic_shop) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, electronic_shop.getName());
            values.put(KEY_ADDRESS, electronic_shop.getAddress());
            values.put(KEY_PHONE, electronic_shop.getPhone());
            values.put(KEY_PHONE1, electronic_shop.getPhone1());
            values.put(KEY_PHONE2, electronic_shop.getPhone2());
            values.put(KEY_WEBSITE, electronic_shop.getWebsite());
            values.put(KEY_LATITUDE, electronic_shop.getLatitude());
            values.put(KEY_LONGITUDE, electronic_shop.getLongitude());
            values.put(KEY_AVATAR, electronic_shop.getAvatar());
            values.put(KEY_AVATAR1, electronic_shop.getAvatar1());
            values.put(KEY_AVATAR2, electronic_shop.getAvatar2());
            values.put(KEY_AVATAR3, electronic_shop.getAvatar3());
            values.put(KEY_AVATAR4, electronic_shop.getAvatar4());
            values.put(KEY_DESC, electronic_shop.getDesc());
            db.insert(TABLE_ELECTRONIC_SHOP, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addtea_shop(Tea_shop tea_shop) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, tea_shop.getName());
            values.put(KEY_ADDRESS, tea_shop.getAddress());
            values.put(KEY_PHONE, tea_shop.getPhone());
            values.put(KEY_PHONE1, tea_shop.getPhone1());
            values.put(KEY_PHONE2, tea_shop.getPhone2());
            values.put(KEY_WEBSITE, tea_shop.getWebsite());
            values.put(KEY_LATITUDE, tea_shop.getLatitude());
            values.put(KEY_LONGITUDE, tea_shop.getLongitude());
            values.put(KEY_AVATAR, tea_shop.getAvatar());
            values.put(KEY_AVATAR1, tea_shop.getAvatar1());
            values.put(KEY_AVATAR2, tea_shop.getAvatar2());
            values.put(KEY_AVATAR3, tea_shop.getAvatar3());
            values.put(KEY_AVATAR4, tea_shop.getAvatar4());
            values.put(KEY_DESC, tea_shop.getDesc());
            db.insert(TABLE_TEA_SHOP, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addcycle_shop(Cycle_shop cycle_shop) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, cycle_shop.getName());
            values.put(KEY_ADDRESS, cycle_shop.getAddress());
            values.put(KEY_PHONE, cycle_shop.getPhone());
            values.put(KEY_PHONE1, cycle_shop.getPhone1());
            values.put(KEY_PHONE2, cycle_shop.getPhone2());
            values.put(KEY_WEBSITE, cycle_shop.getWebsite());
            values.put(KEY_LATITUDE, cycle_shop.getLatitude());
            values.put(KEY_LONGITUDE, cycle_shop.getLongitude());
            values.put(KEY_AVATAR, cycle_shop.getAvatar());
            values.put(KEY_AVATAR1, cycle_shop.getAvatar1());
            values.put(KEY_AVATAR2, cycle_shop.getAvatar2());
            values.put(KEY_AVATAR3, cycle_shop.getAvatar3());
            values.put(KEY_AVATAR4, cycle_shop.getAvatar4());
            values.put(KEY_DESC, cycle_shop.getDesc());
            db.insert(TABLE_CYCLE_SHOP, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addrestaurant(Restaurant restaurant) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, restaurant.getName());
            values.put(KEY_ADDRESS, restaurant.getAddress());
            values.put(KEY_PHONE, restaurant.getPhone());
            values.put(KEY_PHONE1, restaurant.getPhone1());
            values.put(KEY_PHONE2, restaurant.getPhone2());
            values.put(KEY_WEBSITE, restaurant.getWebsite());
            values.put(KEY_LATITUDE, restaurant.getLatitude());
            values.put(KEY_LONGITUDE, restaurant.getLongitude());
            values.put(KEY_AVATAR, restaurant.getAvatar());
            values.put(KEY_AVATAR1, restaurant.getAvatar1());
            values.put(KEY_AVATAR2, restaurant.getAvatar2());
            values.put(KEY_AVATAR3, restaurant.getAvatar3());
            values.put(KEY_AVATAR4, restaurant.getAvatar4());
            values.put(KEY_DESC, restaurant.getDesc());
            db.insert(TABLE_RESTAURANT, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public void addads(Ads ads) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_SHOP_ID, ads.getShop_id());
            values.put(KEY_CATEGORY_ID, ads.getCategory_id());
            values.put(KEY_AVATAR, ads.getAvatar());
            values.put(KEY_MAIN_ADS, ads.getMainAds());
            db.insert(TABLE_ADS, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }


    public void addstickeyads(StickeyAds ads) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_AVATAR, ads.getAvatar());
            db.insert(TABLE_STICKEY_ADS, null, values);
            db.close();
        } catch (Exception e) {
            Log.e("problem", e + "");
        }
    }

    public ArrayList<Bank> getAllbank() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bank> bankList = null;
        try {
            bankList = new ArrayList<Bank>();
            String QUERY = "SELECT * FROM " + TABLE_BANK;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Bank bank = new Bank();
                    bank.setId(cursor.getInt(0));
                    bank.setName(cursor.getString(1));
                    bank.setAddress(cursor.getString(2));
                    bank.setPhone(cursor.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor.getString(6));
                    bank.setLatitude(cursor.getString(7));
                    bank.setLongitude(cursor.getString(8));
                    bank.setAvatar(cursor.getString(9));
                    bank.setAvatar1(cursor.getString(10));
                    bank.setAvatar2(cursor.getString(11));
                    bank.setAvatar3(cursor.getString(12));
                    bank.setAvatar4(cursor.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }



    public ArrayList<Rating> getAllrating(String ratename, String rate_category) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Rating> bankList = null;
        try {
            bankList = new ArrayList<Rating>();
            String QUERY = "SELECT * FROM " + TABLE_RATE + " where " + KEY_RATENAME + " = '" + ratename
                    + "' AND " + KEY_RATECATEGORY + " = '" + rate_category+"'";

//            String QUERY = "SELECT * FROM " + TABLE_RATE + " where " + KEY_RATECATEGORY + " = '" + rate_category+"'";

//            String QUERY = "SELECT * FROM " + TABLE_RATE;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Rating bank = new Rating();
                    bank.setId(cursor.getInt(0));
                    bank.setUser_name(cursor.getString(1));
                    bank.setRate_name(cursor.getString(2));
                    bank.setRate_number(cursor.getString(3));
                    bank.setRate_category(cursor.getString(4));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Bank> getBankDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bank> bankList = null;
        try {
            bankList = new ArrayList<Bank>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_BANK+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Bank bank = new Bank();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }





    public ArrayList<Beauty_saloon> getBeautySaloonDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Beauty_saloon> bankList = null;
        try {
            bankList = new ArrayList<Beauty_saloon>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_BEAUTY_SALOON+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Beauty_saloon bank = new Beauty_saloon();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }



    public ArrayList<Bus_gate> getBusGateDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bus_gate> bankList = null;
        try {
            bankList = new ArrayList<Bus_gate>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_BUS_GATE+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Bus_gate bank = new Bus_gate();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setFrom_township(cursor2.getString(9));
                    bank.setTo_township(cursor2.getString(10));
                    bank.setDeparture_time(cursor2.getString(11));
                    bank.setAvatar(cursor2.getString(12));
                    bank.setAvatar1(cursor2.getString(13));
                    bank.setAvatar2(cursor2.getString(14));
                    bank.setAvatar3(cursor2.getString(15));
                    bank.setAvatar4(cursor2.getString(16));
                    bank.setDesc(cursor.getString(17));

                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }




    public ArrayList<Computer> getComputerDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Computer> bankList = null;
        try {
            bankList = new ArrayList<Computer>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_COMPUTER+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Computer bank = new Computer();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setCategory_type(cursor2.getString(9));
                    bank.setAvatar(cursor2.getString(10));
                    bank.setAvatar1(cursor2.getString(11));
                    bank.setAvatar2(cursor2.getString(12));
                    bank.setAvatar3(cursor2.getString(13));
                    bank.setAvatar4(cursor2.getString(14));
                    bank.setDesc(cursor.getString(15));

                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }




    public String getMainShopNamebyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        String shop_id = null;
        try {
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            shop_id = cursor.getString(0);

        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return shop_id;
    }


    public String getMainTABLEnameDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        String shop_id = null;
        try {
            String QUERY = "SELECT "+ KEY_CATEGORY_ID +" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            shop_id = cursor.getString(0);

        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return shop_id;
    }




    public ArrayList<Cycle_shop> getCycleShopDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Cycle_shop> bankList = null;
        try {
            bankList = new ArrayList<Cycle_shop>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_CYCLE_SHOP+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Cycle_shop bank = new Cycle_shop();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }



    public ArrayList<Education> getEducationDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Education> bankList = null;
        try {
            bankList = new ArrayList<Education>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_EDUCATION+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Education bank = new Education();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Electronic_store> getElectronicShopDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Electronic_store> bankList = null;
        try {
            bankList = new ArrayList<Electronic_store>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_ELECTRONIC_SHOP+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Electronic_store bank = new Electronic_store();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Emergency_contact> getEmergencyContactDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Emergency_contact> bankList = null;
        try {
            bankList = new ArrayList<Emergency_contact>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_EMERGENCY_CONTACT+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Emergency_contact bank = new Emergency_contact();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Famous_place> getFamousPlaceDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Famous_place> bankList = null;
        try {
            bankList = new ArrayList<Famous_place>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_FAMOUS_PLACE+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Famous_place bank = new Famous_place();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Farmer_shop> getFarmerShopDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Farmer_shop> bankList = null;
        try {
            bankList = new ArrayList<Farmer_shop>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_FARMER_SHOP+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Farmer_shop bank = new Farmer_shop();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Gas_station> getGasStationDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Gas_station> bankList = null;
        try {
            bankList = new ArrayList<Gas_station>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_GAS_STATION+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Gas_station bank = new Gas_station();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Hospital> getHospitalDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hospital> bankList = null;
        try {
            bankList = new ArrayList<Hospital>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_HOSPITAL+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Hospital bank = new Hospital();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Jewellery> getJewelleryDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Jewellery> bankList = null;
        try {
            bankList = new ArrayList<Jewellery>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_JEWELLERY+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Jewellery bank = new Jewellery();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Restaurant> getRestaurantDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Restaurant> bankList = null;
        try {
            bankList = new ArrayList<Restaurant>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_RESTAURANT+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Restaurant bank = new Restaurant();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Hotel> getHotelDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hotel> bankList = null;
        try {
            bankList = new ArrayList<Hotel>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_HOTEL+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Hotel bank = new Hotel();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setHoteltype(cursor2.getString(9));
                    bank.setAvatar(cursor2.getString(10));
                    bank.setAvatar1(cursor2.getString(11));
                    bank.setAvatar2(cursor2.getString(12));
                    bank.setAvatar3(cursor2.getString(13));
                    bank.setAvatar4(cursor2.getString(14));
                    bank.setDesc(cursor.getString(15));


                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Shopping_center> getShoppingCenterDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Shopping_center> bankList = null;
        try {
            bankList = new ArrayList<Shopping_center>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_SHOPPING_CENTER+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Shopping_center bank = new Shopping_center();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Store> getStoreDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Store> bankList = null;
        try {
            bankList = new ArrayList<Store>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_STORE+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Store bank = new Store();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Tea_shop> getTeaShopDetailbyAvatar(String avatar_file_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Tea_shop> bankList = null;
        try {
            bankList = new ArrayList<Tea_shop>();
            String QUERY = "SELECT "+ KEY_SHOP_ID+" FROM " + TABLE_ADS + " where " + KEY_AVATAR + " = '" + avatar_file_name+"' ";
            Cursor cursor = db.rawQuery(QUERY, null);

            String QUERY2 = "SELECT * FROM " + TABLE_TEA_SHOP+ " where " + KEY_NAME+ " = '" + cursor.getString(0) +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Tea_shop bank = new Tea_shop();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor.getString(4));
                    bank.setPhone2(cursor.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }




    public ArrayList<Bank> getBankDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bank> bankList = null;
        try {
            bankList = new ArrayList<Bank>();

            String QUERY2 = "SELECT * FROM " + TABLE_BANK+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Bank bank = new Bank();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Beauty_saloon> getBeautySaloonDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Beauty_saloon> bankList = null;
        try {
            bankList = new ArrayList<Beauty_saloon>();

            String QUERY2 = "SELECT * FROM " + TABLE_BEAUTY_SALOON+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Beauty_saloon bank = new Beauty_saloon();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }


    public ArrayList<Bus_gate> getBusGateDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bus_gate> bankList = null;
        try {
            bankList = new ArrayList<Bus_gate>();

            String QUERY2 = "SELECT * FROM " + TABLE_BUS_GATE+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Bus_gate bank = new Bus_gate();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setFrom_township(cursor2.getString(9));
                    bank.setTo_township(cursor2.getString(10));
                    bank.setDeparture_time(cursor2.getString(11));
                    bank.setAvatar(cursor2.getString(12));
                    bank.setAvatar1(cursor2.getString(13));
                    bank.setAvatar2(cursor2.getString(14));
                    bank.setAvatar3(cursor2.getString(15));
                    bank.setAvatar4(cursor2.getString(16));
                    bank.setDesc(cursor2.getString(17));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Computer> getComputerDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Computer> bankList = null;
        try {
            bankList = new ArrayList<Computer>();

            String QUERY2 = "SELECT * FROM " + TABLE_COMPUTER+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Computer bank = new Computer();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setCategory_type(cursor2.getString(9));
                    bank.setAvatar(cursor2.getString(10));
                    bank.setAvatar1(cursor2.getString(11));
                    bank.setAvatar2(cursor2.getString(12));
                    bank.setAvatar3(cursor2.getString(13));
                    bank.setAvatar4(cursor2.getString(14));
                    bank.setDesc(cursor2.getString(15));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Cycle_shop> getCycleShopDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Cycle_shop> bankList = null;
        try {
            bankList = new ArrayList<Cycle_shop>();

            String QUERY2 = "SELECT * FROM " + TABLE_CYCLE_SHOP+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Cycle_shop bank = new Cycle_shop();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Education> getEducationDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Education> bankList = null;
        try {
            bankList = new ArrayList<Education>();

            String QUERY2 = "SELECT * FROM " + TABLE_EDUCATION+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Education bank = new Education();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Electronic_store> getElectronicShopDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Electronic_store> bankList = null;
        try {
            bankList = new ArrayList<Electronic_store>();

            String QUERY2 = "SELECT * FROM " + TABLE_ELECTRONIC_SHOP+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Electronic_store bank = new Electronic_store();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Emergency_contact> getEmergencyContactDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Emergency_contact> bankList = null;
        try {
            bankList = new ArrayList<Emergency_contact>();

            String QUERY2 = "SELECT * FROM " + TABLE_EMERGENCY_CONTACT+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Emergency_contact bank = new Emergency_contact();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Famous_place> getFamousPlaceDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Famous_place> bankList = null;
        try {
            bankList = new ArrayList<Famous_place>();

            String QUERY2 = "SELECT * FROM " + TABLE_FAMOUS_PLACE+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Famous_place bank = new Famous_place();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Farmer_shop> getFarmerShopDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Farmer_shop> bankList = null;
        try {
            bankList = new ArrayList<Farmer_shop>();

            String QUERY2 = "SELECT * FROM " + TABLE_FARMER_SHOP+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Farmer_shop bank = new Farmer_shop();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Gas_station> getGasStationDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Gas_station> bankList = null;
        try {
            bankList = new ArrayList<Gas_station>();

            String QUERY2 = "SELECT * FROM " + TABLE_GAS_STATION+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Gas_station bank = new Gas_station();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Hospital> getHospitalDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hospital> bankList = null;
        try {
            bankList = new ArrayList<Hospital>();

            String QUERY2 = "SELECT * FROM " + TABLE_HOSPITAL+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Hospital bank = new Hospital();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Jewellery> getJewelleryDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Jewellery> bankList = null;
        try {
            bankList = new ArrayList<Jewellery>();

            String QUERY2 = "SELECT * FROM " + TABLE_JEWELLERY+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Jewellery bank = new Jewellery();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Restaurant> getRestaurantDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Restaurant> bankList = null;
        try {
            bankList = new ArrayList<Restaurant>();

            String QUERY2 = "SELECT * FROM " + TABLE_RESTAURANT+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Restaurant bank = new Restaurant();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Hotel> getHotelDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hotel> bankList = null;
        try {
            bankList = new ArrayList<Hotel>();

            String QUERY2 = "SELECT * FROM " + TABLE_HOTEL+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Hotel bank = new Hotel();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setHoteltype(cursor2.getString(9));
                    bank.setAvatar(cursor2.getString(10));
                    bank.setAvatar1(cursor2.getString(11));
                    bank.setAvatar2(cursor2.getString(12));
                    bank.setAvatar3(cursor2.getString(13));
                    bank.setAvatar4(cursor2.getString(14));
                    bank.setDesc(cursor2.getString(15));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Shopping_center> getShopping_centerDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Shopping_center> bankList = null;
        try {
            bankList = new ArrayList<Shopping_center>();

            String QUERY2 = "SELECT * FROM " + TABLE_SHOPPING_CENTER+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Shopping_center bank = new Shopping_center();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Store> getStoreDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Store> bankList = null;
        try {
            bankList = new ArrayList<Store>();

            String QUERY2 = "SELECT * FROM " + TABLE_STORE+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Store bank = new Store();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Tea_shop> getTeaShopDetailbyshop_id(String shop_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Tea_shop> bankList = null;
        try {
            bankList = new ArrayList<Tea_shop>();

            String QUERY2 = "SELECT * FROM " + TABLE_TEA_SHOP+ " where " + KEY_NAME+ " = '" + shop_id +"' ";
            Cursor cursor2 = db.rawQuery(QUERY2, null);

            if (!cursor2.isLast()) {
                while (cursor2.moveToNext()) {
                    Tea_shop bank = new Tea_shop();
                    bank.setId(cursor2.getInt(0));
                    bank.setName(cursor2.getString(1));
                    bank.setAddress(cursor2.getString(2));
                    bank.setPhone(cursor2.getString(3));
                    bank.setPhone1(cursor2.getString(4));
                    bank.setPhone2(cursor2.getString(5));
                    bank.setWebsite(cursor2.getString(6));
                    bank.setLatitude(cursor2.getString(7));
                    bank.setLongitude(cursor2.getString(8));
                    bank.setAvatar(cursor2.getString(9));
                    bank.setAvatar1(cursor2.getString(10));
                    bank.setAvatar2(cursor2.getString(11));
                    bank.setAvatar3(cursor2.getString(12));
                    bank.setAvatar4(cursor2.getString(13));
                    bank.setDesc(cursor2.getString(14));
                    bankList.add(bank);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bankList;
    }

    public ArrayList<Computer> getAllcomputer() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Computer> computerList = null;
        try {
            computerList = new ArrayList<Computer>();
            String QUERY = "SELECT * FROM " + TABLE_COMPUTER;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Computer computer = new Computer();
                    computer.setId(cursor.getInt(0));
                    computer.setName(cursor.getString(1));
                    computer.setAddress(cursor.getString(2));
                    computer.setPhone(cursor.getString(3));
                    computer.setPhone1(cursor.getString(4));
                    computer.setPhone2(cursor.getString(5));
                    computer.setWebsite(cursor.getString(6));
                    computer.setLatitude(cursor.getString(7));
                    computer.setLongitude(cursor.getString(8));
                    computer.setCategory_type(cursor.getString(9));
                    computer.setAvatar(cursor.getString(10));
                    computer.setAvatar1(cursor.getString(11));
                    computer.setAvatar2(cursor.getString(12));
                    computer.setAvatar3(cursor.getString(13));
                    computer.setAvatar4(cursor.getString(14));
                    computer.setDesc(cursor.getString(15));
                    computerList.add(computer);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return computerList;
    }


    public ArrayList<Hotel> getAllhotel() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hotel> hotelList = null;
        try {
            hotelList = new ArrayList<Hotel>();
            String QUERY = "SELECT * FROM " + TABLE_HOTEL;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Hotel hotel = new Hotel();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setHoteltype(cursor.getString(9));
                    hotel.setAvatar(cursor.getString(10));
                    hotel.setAvatar1(cursor.getString(11));
                    hotel.setAvatar2(cursor.getString(12));
                    hotel.setAvatar3(cursor.getString(13));
                    hotel.setAvatar4(cursor.getString(14));
                    hotel.setDesc(cursor.getString(15));
                    hotelList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return hotelList;
    }


    public ArrayList<Education> getAlleducation() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Education> educationList = null;
        try {
            educationList = new ArrayList<Education>();
            String QUERY = "SELECT * FROM " + TABLE_EDUCATION;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Education hotel = new Education();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    educationList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return educationList;
    }

    public ArrayList<Hospital> getAllhospital() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hospital> hospitalList = null;
        try {
            hospitalList = new ArrayList<Hospital>();
            String QUERY = "SELECT * FROM " + TABLE_HOSPITAL;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Hospital hotel = new Hospital();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    hospitalList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return hospitalList;
    }

    public ArrayList<Store> getAllstore() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Store> storeList = null;
        try {
            storeList = new ArrayList<Store>();
            String QUERY = "SELECT * FROM " + TABLE_STORE;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Store hotel = new Store();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    storeList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return storeList;
    }

    public ArrayList<Famous_place> getAllfamous_place() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Famous_place> famous_placeList = null;
        try {
            famous_placeList = new ArrayList<Famous_place>();
            String QUERY = "SELECT * FROM " + TABLE_FAMOUS_PLACE;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Famous_place hotel = new Famous_place();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    famous_placeList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return famous_placeList;
    }

    public ArrayList<Gas_station> getAllgas_station() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Gas_station> gas_stationList = null;
        try {
            gas_stationList = new ArrayList<Gas_station>();
            String QUERY = "SELECT * FROM " + TABLE_GAS_STATION;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Gas_station hotel = new Gas_station();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    gas_stationList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return gas_stationList;
    }

    public ArrayList<Jewellery> getAlljewellery() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Jewellery> jewelleryList = null;
        try {
            jewelleryList = new ArrayList<Jewellery>();
            String QUERY = "SELECT * FROM " + TABLE_JEWELLERY;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Jewellery hotel = new Jewellery();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    jewelleryList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return jewelleryList;
    }

    public ArrayList<Shopping_center> getAllshopping_centerr() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Shopping_center> shopping_centerrList = null;
        try {
            shopping_centerrList = new ArrayList<Shopping_center>();
            String QUERY = "SELECT * FROM " + TABLE_SHOPPING_CENTER;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Shopping_center hotel = new Shopping_center();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    shopping_centerrList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return shopping_centerrList;
    }

    public ArrayList<Emergency_contact> getAllemergency_contact() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Emergency_contact> emergency_contactList = null;
        try {
            emergency_contactList = new ArrayList<Emergency_contact>();
            String QUERY = "SELECT * FROM " + TABLE_EMERGENCY_CONTACT;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Emergency_contact hotel = new Emergency_contact();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    emergency_contactList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return emergency_contactList;
    }

    public ArrayList<Farmer_shop> getAllfarmer_shop() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Farmer_shop> farmer_shopList = null;
        try {
            farmer_shopList = new ArrayList<Farmer_shop>();
            String QUERY = "SELECT * FROM " + TABLE_FARMER_SHOP;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Farmer_shop hotel = new Farmer_shop();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    farmer_shopList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return farmer_shopList;
    }

    public ArrayList<Beauty_saloon> getAllbeauty_saloon() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Beauty_saloon> beauty_saloonList = null;
        try {
            beauty_saloonList = new ArrayList<Beauty_saloon>();
            String QUERY = "SELECT * FROM " + TABLE_BEAUTY_SALOON;
            Cursor cursor = db.rawQuery(QUERY, null);

            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Beauty_saloon hotel = new Beauty_saloon();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    beauty_saloonList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return beauty_saloonList;
    }

    public ArrayList<Bus_gate> getAllbus_gate() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bus_gate> bus_gateList = null;
        try {
            bus_gateList = new ArrayList<Bus_gate>();
            String QUERY = "SELECT * FROM " + TABLE_BUS_GATE;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Bus_gate hotel = new Bus_gate();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setFrom_township(cursor.getString(9));
                    hotel.setTo_township(cursor.getString(10));
                    hotel.setDeparture_time(cursor.getString(11));
                    hotel.setAvatar(cursor.getString(12));
                    hotel.setAvatar1(cursor.getString(13));
                    hotel.setAvatar2(cursor.getString(14));
                    hotel.setAvatar3(cursor.getString(15));
                    hotel.setAvatar4(cursor.getString(16));
                    hotel.setDesc(cursor.getString(17));
                    bus_gateList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return bus_gateList;
    }

    public ArrayList<Electronic_store> getAllelectronic_shop() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Electronic_store> electronic_shopList = null;
        try {
            electronic_shopList = new ArrayList<Electronic_store>();
            String QUERY = "SELECT * FROM " + TABLE_ELECTRONIC_SHOP;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Electronic_store hotel = new Electronic_store();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    electronic_shopList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return electronic_shopList;
    }

    public ArrayList<Tea_shop> getAlltea_shop() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Tea_shop> tea_shopList = null;
        try {
            tea_shopList = new ArrayList<Tea_shop>();
            String QUERY = "SELECT * FROM " + TABLE_TEA_SHOP;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Tea_shop hotel = new Tea_shop();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    tea_shopList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return tea_shopList;
    }

    public ArrayList<Cycle_shop> getAllcycle_shop() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Cycle_shop> cycle_shopList = null;
        try {
            cycle_shopList = new ArrayList<Cycle_shop>();
            String QUERY = "SELECT * FROM " + TABLE_CYCLE_SHOP;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Cycle_shop hotel = new Cycle_shop();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    cycle_shopList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return cycle_shopList;
    }

    public ArrayList<Restaurant> getAllrestaurant() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Restaurant> restaurantList = null;
        try {
            restaurantList = new ArrayList<Restaurant>();
            String QUERY = "SELECT * FROM " + TABLE_RESTAURANT;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Restaurant hotel = new Restaurant();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    restaurantList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return restaurantList;
    }

    public ArrayList<Ads> getAllads() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Ads> restaurantList = null;
        try {
            restaurantList = new ArrayList<Ads>();
            String QUERY = "SELECT * FROM " + TABLE_ADS;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Ads hotel = new Ads();
                    hotel.setId(cursor.getInt(0));
                    hotel.setShop_id(cursor.getString(1));
                    hotel.setCategory_id(cursor.getString(2));
                    hotel.setAvatar(cursor.getString(3));
                    restaurantList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return restaurantList;
    }



    public ArrayList<StickeyAds> getAllstickeyads() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StickeyAds> restaurantList = null;
        try {
            restaurantList = new ArrayList<StickeyAds>();
            String QUERY = "SELECT * FROM " + TABLE_STICKEY_ADS;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    StickeyAds hotel = new StickeyAds();
                    hotel.setId(cursor.getInt(0));
                    hotel.setAvatar(cursor.getString(1));
                    restaurantList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return restaurantList;
    }


    public ArrayList<Ads> getAlladsbyCategory(String category_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Ads> restaurantList = null;
        try {
            restaurantList = new ArrayList<Ads>();

            String QUERY = "SELECT * FROM " + TABLE_ADS + " where " + KEY_CATEGORY_ID + " = '" + category_name + "'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Ads hotel = new Ads();
                    hotel.setId(cursor.getInt(0));
                    hotel.setShop_id(cursor.getString(1));
                    hotel.setCategory_id(cursor.getString(2));
                    hotel.setAvatar(cursor.getString(3));
                    restaurantList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return restaurantList;
    }

    public ArrayList<Ads> getMainadsbyCategory() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Ads> restaurantList = null;
        try {
            restaurantList = new ArrayList<Ads>();

            String QUERY = "SELECT * FROM " + TABLE_ADS + " where " + KEY_MAIN_ADS + " = 'yes'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Ads hotel = new Ads();
                    hotel.setId(cursor.getInt(0));
                    hotel.setShop_id(cursor.getString(1));
                    hotel.setCategory_id(cursor.getString(2));
                    hotel.setAvatar(cursor.getString(3));
                    hotel.setMain_ads(cursor.getString(4));
                    restaurantList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return restaurantList;
    }



    public ArrayList<Jewellery> getsearchedjewellery(String searched_key_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Jewellery> jewelleryList = null;
        try {
            jewelleryList = new ArrayList<Jewellery>();
            String QUERY = "SELECT * FROM " + TABLE_JEWELLERY + " where LOWER(" + KEY_NAME + ") = '" + searched_key_name +"'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Jewellery hotel = new Jewellery();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    jewelleryList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return jewelleryList;
    }


    public ArrayList<Beauty_saloon> getsearchedbeautysaloon(String searched_key_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Beauty_saloon> jewelleryList = null;
        try {
            jewelleryList = new ArrayList<Beauty_saloon>();
            String QUERY = "SELECT * FROM " + TABLE_BEAUTY_SALOON + " where LOWER(" + KEY_NAME + ") = '" + searched_key_name.toLowerCase() +"'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Beauty_saloon hotel = new Beauty_saloon();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    jewelleryList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return jewelleryList;
    }


    public ArrayList<Bus_gate> getsearchedbusgate(String from_township, String to_township) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Bus_gate> jewelleryList = null;
        try {
            jewelleryList = new ArrayList<Bus_gate>();
            String QUERY = "SELECT * FROM " + TABLE_BUS_GATE + " where " + KEY_FROM_TOWNSHIP + " = '"
                    + from_township + "' and " + KEY_TO_TOWNSHIP + " = '" + to_township +"'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Bus_gate hotel = new Bus_gate();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setAvatar(cursor.getString(9));
                    hotel.setAvatar1(cursor.getString(10));
                    hotel.setAvatar2(cursor.getString(11));
                    hotel.setAvatar3(cursor.getString(12));
                    hotel.setAvatar4(cursor.getString(13));
                    hotel.setDesc(cursor.getString(14));
                    jewelleryList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return jewelleryList;
    }


    public ArrayList<Hotel> gethotelbycategory(String category_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Hotel> jewelleryList = null;
        try {
            jewelleryList = new ArrayList<Hotel>();
            String QUERY = "SELECT * FROM " + TABLE_HOTEL + " where " + KEY_HOTELTYPE + " = '"
                    + category_name +"'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Hotel hotel = new Hotel();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setHoteltype(cursor.getString(9));
                    hotel.setAvatar(cursor.getString(10));
                    hotel.setAvatar1(cursor.getString(11));
                    hotel.setAvatar2(cursor.getString(12));
                    hotel.setAvatar3(cursor.getString(13));
                    hotel.setAvatar4(cursor.getString(14));
                    hotel.setDesc(cursor.getString(15));
                    jewelleryList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return jewelleryList;
    }

    public ArrayList<Computer> getcomputerbycategory(String category_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Computer> jewelleryList = null;
        try {
            jewelleryList = new ArrayList<Computer>();
            String QUERY = "SELECT * FROM " + TABLE_COMPUTER + " where " + KEY_COMPUTERTYPE+ " = '"
                    + category_name +"'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Computer hotel = new Computer();
                    hotel.setId(cursor.getInt(0));
                    hotel.setName(cursor.getString(1));
                    hotel.setAddress(cursor.getString(2));
                    hotel.setPhone(cursor.getString(3));
                    hotel.setPhone1(cursor.getString(4));
                    hotel.setPhone2(cursor.getString(5));
                    hotel.setWebsite(cursor.getString(6));
                    hotel.setLatitude(cursor.getString(7));
                    hotel.setLongitude(cursor.getString(8));
                    hotel.setCategory_type(cursor.getString(9));
                    hotel.setAvatar(cursor.getString(10));
                    hotel.setAvatar1(cursor.getString(11));
                    hotel.setAvatar2(cursor.getString(12));
                    hotel.setAvatar3(cursor.getString(13));
                    hotel.setAvatar4(cursor.getString(14));
                    hotel.setDesc(cursor.getString(15));
                    jewelleryList.add(hotel);
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return jewelleryList;
    }


    public List<String> getall_to_township() {
        SQLiteDatabase db = this.getReadableDatabase();
        try {

            String QUERY = "SELECT distinct "+ KEY_TO_TOWNSHIP +" FROM " + TABLE_BUS_GATE;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    to_region_list.add(cursor.getString(0));
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return to_region_list;
    }


    public List<String> getall_from_township() {
        SQLiteDatabase db = this.getReadableDatabase();
        try {

            String QUERY = "SELECT distinct "+ KEY_FROM_TOWNSHIP +" FROM " + TABLE_BUS_GATE;
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    from_region_list.add(cursor.getString(0));
                }
            }
            db.close();
        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return from_region_list;
    }

    public int getTableCount(String category_name) {
        int num = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            if (category_name.equals("bank")) {
                QUERY = "SELECT * FROM " + TABLE_BANK;
            } else if (category_name.equals("computer")) {
                QUERY = "SELECT * FROM " + TABLE_COMPUTER;
            } else if (category_name.equals("hotel")) {
                QUERY = "SELECT * FROM " + TABLE_HOTEL;
            } else if (category_name.equals("education")) {
                QUERY = "SELECT * FROM " + TABLE_EDUCATION;
            } else if (category_name.equals("hospital")) {
                QUERY = "SELECT * FROM " + TABLE_HOSPITAL;
            } else if (category_name.equals("store")) {
                QUERY = "SELECT * FROM " + TABLE_STORE;
            } else if (category_name.equals("famous_place")) {
                QUERY = "SELECT * FROM " + TABLE_FAMOUS_PLACE;
            } else if (category_name.equals("gas_station")) {
                QUERY = "SELECT * FROM " + TABLE_GAS_STATION;
            } else if (category_name.equals("jewellery")) {
                QUERY = "SELECT * FROM " + TABLE_JEWELLERY;
            } else if (category_name.equals("shopping_center")) {
                QUERY = "SELECT * FROM " + TABLE_SHOPPING_CENTER;
            } else if (category_name.equals("emergency_contact")) {
                QUERY = "SELECT * FROM " + TABLE_EMERGENCY_CONTACT;
            } else if (category_name.equals("farmer_shop")) {
                QUERY = "SELECT * FROM " + TABLE_FARMER_SHOP;
            } else if (category_name.equals("beauty_saloon")) {
                QUERY = "SELECT * FROM " + TABLE_BEAUTY_SALOON;
            } else if (category_name.equals("bus_gate")) {
                QUERY = "SELECT * FROM " + TABLE_BUS_GATE;
            } else if (category_name.equals("electronic_store")) {
                QUERY = "SELECT * FROM " + TABLE_ELECTRONIC_SHOP;
            } else if (category_name.equals("tea_shop")) {
                QUERY = "SELECT * FROM " + TABLE_TEA_SHOP;
            } else if (category_name.equals("cycle_shop")) {
                QUERY = "SELECT * FROM " + TABLE_CYCLE_SHOP;
            } else if (category_name.equals("restaurant")) {
                QUERY = "SELECT * FROM " + TABLE_RESTAURANT;
            } else if (category_name.equals("ads")) {
                QUERY = "SELECT * FROM " + TABLE_ADS;
            } else if (category_name.equals("stickey-ads")) {
                QUERY = "SELECT * FROM " + TABLE_STICKEY_ADS;
            }

            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;


        } catch (Exception e) {
            Log.e("error", e + "");
        }
        return 0;
    }


    public void deleteData(String category_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            if (category_name.equals("bank")) {
//                QUERY = "delete from "+ TABLE_BANK;
                db.delete(TABLE_BANK, null, null);
            } else if (category_name.equals("computer")) {
//                QUERY = "delete FROM " + TABLE_COMPUTER;
                db.delete(TABLE_COMPUTER, null, null);
            } else if (category_name.equals("hotel")) {
//                QUERY = "delete FROM " + TABLE_HOTEL;
                db.delete(TABLE_HOTEL, null, null);
            } else if (category_name.equals("education")) {
//                QUERY = "delete FROM " + TABLE_EDUCATION;
                db.delete(TABLE_EDUCATION, null, null);
            } else if (category_name.equals("hospital")) {
//                QUERY = "delete FROM " + TABLE_HOSPITAL;
                db.delete(TABLE_HOSPITAL, null, null);
            } else if (category_name.equals("store")) {
//                QUERY = "delete FROM " + TABLE_STORE;
                db.delete(TABLE_STORE, null, null);
            } else if (category_name.equals("famous_place")) {
//                QUERY = "delete FROM " + TABLE_FAMOUS_PLACE;
                db.delete(TABLE_FAMOUS_PLACE, null, null);
            } else if (category_name.equals("gas_station")) {
//                QUERY = "delete FROM " + TABLE_GAS_STATION;
                db.delete(TABLE_GAS_STATION, null, null);
            } else if (category_name.equals("jewellery")) {
//                QUERY = "delete FROM " + TABLE_JEWELLERY;
                db.delete(TABLE_JEWELLERY, null, null);
            } else if (category_name.equals("shopping_center")) {
//                QUERY = "delete FROM " + TABLE_SHOPPING_CENTER;
                db.delete(TABLE_SHOPPING_CENTER, null, null);
            } else if (category_name.equals("emergency_contact")) {
//                QUERY = "delete FROM " + TABLE_EMERGENCY_CONTACT;
                db.delete(TABLE_EMERGENCY_CONTACT, null, null);
            } else if (category_name.equals("farmer_shop")) {
//                QUERY = "delete FROM " + TABLE_FARMER_SHOP;
                db.delete(TABLE_FARMER_SHOP, null, null);
            } else if (category_name.equals("beauty_saloon")) {
//                QUERY = "delete FROM " + TABLE_BEAUTY_SALOON;
                db.delete(TABLE_BEAUTY_SALOON, null, null);
            } else if (category_name.equals("bus_gate")) {
//                QUERY = "delete FROM " + TABLE_BUS_GATE;
                db.delete(TABLE_BUS_GATE, null, null);
            } else if (category_name.equals("electronic_store")) {
//                QUERY = "delete FROM " + TABLE_ELECTRONIC_SHOP;
                db.delete(TABLE_ELECTRONIC_SHOP, null, null);
            } else if (category_name.equals("tea_shop")) {
//                QUERY = "delete FROM " + TABLE_TEA_SHOP;
                db.delete(TABLE_TEA_SHOP, null, null);
            } else if (category_name.equals("cycle_shop")) {
//                QUERY = "delete FROM " + TABLE_CYCLE_SHOP;
                db.delete(TABLE_CYCLE_SHOP, null, null);
            } else if (category_name.equals("restaurant")) {
//                QUERY = "delete FROM " + TABLE_RESTAURANT;
                db.delete(TABLE_RESTAURANT, null, null);
            } else if (category_name.equals("ads_bank")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= bank", null);
            } else if (category_name.equals("ads_beauty_saloon")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= beauty_saloon", null);
            } else if (category_name.equals("ads_bus")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= bus", null);
            } else if (category_name.equals("ads_computer")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= computer", null);
            } else if (category_name.equals("ads_cycle")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= cycle", null);
            } else if (category_name.equals("ads_education")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= education", null);
            } else if (category_name.equals("ads_electronic")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= electronic", null);
            } else if (category_name.equals("ads_emergency_contact")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= emergency_contact", null);
            } else if (category_name.equals("ads_famous_place")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= famous_place", null);
            } else if (category_name.equals("ads_farmer")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= farmer", null);
            } else if (category_name.equals("ads_gas_station")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= gas_station", null);
            } else if (category_name.equals("ads_hospital")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= hospital", null);
            } else if (category_name.equals("ads_jewellery")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= jewellery", null);
            } else if (category_name.equals("ads_restaurant")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= restaurant", null);
            } else if (category_name.equals("ads_hotel")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= hotel", null);
            } else if (category_name.equals("ads_shopping_mall")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= shopping_mall", null);
            } else if (category_name.equals("ads_store")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= store", null);
            } else if (category_name.equals("ads_tea_shop")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_CATEGORY_ID + "= tea_shop", null);
            } else if (category_name.equals("ads_main")) {
//                QUERY = "delete FROM " + TABLE_ADS;
                db.delete(TABLE_ADS, KEY_MAIN_ADS + "= yes", null);
            }else if (category_name.equals("rating")) {
//                QUERY = "delete FROM " + TABLE_RATE;
                db.delete(TABLE_RATE, null, null);
            }else if (category_name.equals("stickey_ads")) {
//                QUERY = "delete FROM " + TABLE_STICKEY_ADS;
                db.delete(TABLE_STICKEY_ADS, null, null);
            }

            db.execSQL(QUERY);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }
}

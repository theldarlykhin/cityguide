package com.hnttechs.www.cityguide;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by dell on 9/26/16.
 */
public class SplashScreenActivity extends Activity {

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        CountDown _tik;
        _tik=new CountDown(4000,4000,this,DrawerActivity.class);
        _tik.start();

        StartAnimations();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        RelativeLayout l=(RelativeLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        TextView lbl_app_name = (TextView)findViewById(R.id.lbl_app_name);
        lbl_app_name.clearAnimation();
        lbl_app_name.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.second_anim);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.img_splash_logo);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }
}

package com.hnttechs.www.cityguide;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Register extends ActionBarActivity {

    ProgressDialog dialog;
    AlertDialog alertDialog;
    static EditText txt_name;
    static EditText txt_email;
    static EditText txt_password;
    static EditText txt_confirm_password;
    static Button btn_register;
    String name,email,password,confirm_password;
    static ConnectivityManager connec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        connec = (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        txt_name = (EditText)findViewById(R.id.userName);
        txt_name.requestFocus();
        txt_email = (EditText)findViewById(R.id.userEmail);
        txt_password = (EditText)findViewById(R.id.userPassword);
        txt_confirm_password = (EditText)findViewById(R.id.userConfirmPassword);
        btn_register = (Button)findViewById(R.id.registerButton);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetOn() == true) {
                    name = txt_name.getText().toString();
                    email= txt_email.getText().toString();
                    password= txt_password.getText().toString();
                    confirm_password= txt_confirm_password.getText().toString();

                    if(name.isEmpty()==true)
                    {

                        Toast.makeText(getBaseContext(),"Please enter your name.",Toast.LENGTH_SHORT).show();

                    } else if(email.isEmpty()==true || email.length()<8) {

                        Toast.makeText(getBaseContext(),"Please enter valid phone no.",Toast.LENGTH_SHORT).show();
                    } else if (password.isEmpty()==true || password.length()<8)
                    {
                        Toast.makeText(getBaseContext(),"Please enter valid password.",Toast.LENGTH_SHORT).show();

                    } else if (confirm_password.isEmpty()==true)
                    {
                        Toast.makeText(getBaseContext(),"Please confirm password again.",Toast.LENGTH_SHORT).show();
                    } else if (!password.equals(confirm_password))
                    {
                        Toast.makeText(getBaseContext(),"Password and confirm password does not match.",Toast.LENGTH_SHORT).show();

                    }
                    else {
                        sendPostRequest(email, password, confirm_password);
                        dialog = ProgressDialog.show(Register.this, "",
                                "Sending...", true);
                        dialog.show();
                    }
                }
                else{

                    Toast.makeText(getBaseContext(),"Please Switch Your Internet Connection and try again.",Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getBaseContext().getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }

    private void sendPostRequest(String email, String password, String password_confirmation) {

        class SendPostReqAsyncTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                String paramEmail = (String) params[0];
                String paramPassword= (String) params[1];
                String paramPasswordConfirm = (String) params[2];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://45.120.149.8/api/v1/user_new");

                BasicNameValuePair emailBasicNameValuePAir = new BasicNameValuePair("email", paramEmail);
                BasicNameValuePair passwordBasicNameValuePAir = new BasicNameValuePair("password", paramPassword);
                BasicNameValuePair passwordConfirmBasicNameValuePAir = new BasicNameValuePair("password_confirmation", paramPasswordConfirm);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(emailBasicNameValuePAir);
                nameValuePairList.add(passwordBasicNameValuePAir);
                nameValuePairList.add(passwordConfirmBasicNameValuePAir);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();
                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                dialog.dismiss();
                finish();
            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(email, password, password_confirmation);
    }
}

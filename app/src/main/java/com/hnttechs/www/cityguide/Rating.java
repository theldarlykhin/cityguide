package com.hnttechs.www.cityguide;

/**
 * Created by dell on 8/20/16.
 */
public class Rating {
    private int id;
    private String user_name;
    private String rate_name;
    private String rate_number;
    private String rate_category;

    public Rating() {
    }

    public Rating(String user_name, String rate_name, String rate_number, String rate_category) {
        this.user_name = user_name;
        this.rate_name = rate_name;
        this.rate_number = rate_number;
        this.rate_category = rate_category;
    }

    public Rating(int id, String user_name, String rate_name, String rate_number, String rate_category) {
        id = id;
        this.user_name = user_name;
        this.rate_name = rate_name;
        this.rate_number = rate_number;
        this.rate_category = rate_category;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name= user_name;
    }

    public String getRate_name() {
        return rate_name;
    }

    public void setRate_name(String rate_name) {
        this.rate_name= rate_name;
    }

    public String getRate_number() {
        return rate_number;
    }

    public void setRate_number(String rate_number) {
        this.rate_number = rate_number;
    }

    public String getRate_category() {
        return rate_category;
    }

    public void setRate_category(String rate_category) {
        this.rate_category = rate_category;
    }
}

package com.hnttechs.www.cityguide;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapterTeaShop extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;

    ArrayList<Tea_shop> bank_listData;

    public ListViewAdapterTeaShop(Context context, ArrayList<Tea_shop> listData) {
        this.context = context;
        bank_listData = listData;

    }

    @Override
    public int getCount() {
        return bank_listData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.simple_list_item, parent, false);
        }



        Drawable mDrawable_phone_icon = context.getResources().getDrawable(R.drawable.phone_icon);
        mDrawable_phone_icon.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#fecd28"), PorterDuff.Mode.MULTIPLY));

        Drawable mDrawable_web_icon = context.getResources().getDrawable(R.drawable.desc);
        mDrawable_web_icon.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor("#fecd28"), PorterDuff.Mode.MULTIPLY));


        Typeface tf_zawgyi = Typeface.createFromAsset(context.getAssets(), "fonts/zawgyione.ttf");

        final TextView txt_name = (TextView) convertView.findViewById(R.id.name);
        final TextView txt_address = (TextView) convertView.findViewById(R.id.address);
        final TextView txt_phone_no = (TextView) convertView.findViewById(R.id.phone_no);
        final TextView txt_phone_no2 = (TextView) convertView.findViewById(R.id.phone_no2);
        final TextView txt_phone_no3 = (TextView) convertView.findViewById(R.id.phone_no3);
        final ImageView img_phone_icon = (ImageView)convertView.findViewById(R.id.img_phone_icon);
        final ImageView img_phone_icon2 = (ImageView)convertView.findViewById(R.id.img_phone_icon2);
        final ImageView img_phone_icon3 = (ImageView)convertView.findViewById(R.id.img_phone_icon3);

        img_phone_icon.setImageDrawable(mDrawable_phone_icon);
        img_phone_icon2.setImageDrawable(mDrawable_phone_icon);
        img_phone_icon3.setImageDrawable(mDrawable_phone_icon);

        Tea_shop bank = bank_listData.get(position);

        txt_name.setTypeface(tf_zawgyi);
        txt_address.setTypeface(tf_zawgyi);
        txt_phone_no.setTypeface(tf_zawgyi);

        String name = bank.getName();
        String address = bank.getAddress();
        String phone = bank.getPhone();
        String phone2 = bank.getPhone1();
        String phone3 = bank.getPhone2();

        txt_name.setText(name);
        txt_address.setText(address);
        txt_phone_no.setText(phone);
        txt_phone_no2.setText(phone2);
        txt_phone_no3.setText(phone3);
        return convertView;
    }
}